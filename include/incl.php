<?php

/*
  WineHQ
  by Jeremy Newman <jnewman@codeweavers.com>
*/

/*
 * Main Include Library
 */

// Require PHP version 5.4 or higher
if (version_compare(phpversion(), '5.4.0') < 0)
{
    trigger_error("PHP 5.4 or Higher Required!", E_USER_ERROR);
}

// load global functions lib (needed before all other libs)
require_once("{$file_root}/include/utils.php");

// script execution start time
$script_start_time = microtime_float();

// Set Up the Class AutoLoader
spl_autoload_register("check_and_require");

// create config object
$config = new config("{$file_root}/include/winehq.conf", "{$file_root}/include/globals.conf");

// start the debug log
$debug_log = "";
debug("global", "Starting Debug Log");

// create html object
$html = new html($file_root);

// setup html error handler when not at CLI
if (!defined('STDIN'))
{
    set_error_handler(array(&$html, 'error_handler'));
    register_shutdown_function(array(&$html, 'error_shutdown'));
}

// Clean XSS attempts from the global user input vars
if (!empty($_SERVER['REQUEST_URI']))
    $_SERVER['REQUEST_URI']  = $html->clean_input($_SERVER['REQUEST_URI']);
if (!empty($_SERVER['HTTP_REFERER']))
    $_SERVER['HTTP_REFERER'] = $html->clean_input($_SERVER['HTTP_REFERER']);
if (!empty($_SERVER['PHP_SELF']))
    $_SERVER['PHP_SELF']     = $html->clean_input($_SERVER['PHP_SELF']);
if (!empty($_SERVER['PHP_SELF_FULL']))
    $_SERVER['PHP_SELF_FULL']     = $html->clean_input($_SERVER['PHP_SELF_FULL']);
if (!empty($_REQUEST))
    $_REQUEST = $html->clean_input($_REQUEST);
if (!empty($_POST))
    $_POST    = $html->clean_input($_POST);
if (!empty($_GET))
    $_GET     = $html->clean_input($_GET);

// skip connecting to session server and DB if in offline mode
if (!empty($config->offline))
{
    debug("global", "Offline Mode");
}
else
{
    // default from config
    $lang = $GLOBALS['config']->lang;

    // load language from cookie
    if (isset($_COOKIE['lang']) and in_array($_COOKIE['lang'], $GLOBALS['config']->languages))
    {
        debug("global", "lang from cookie: {$_COOKIE['lang']}");
        $lang = $_COOKIE['lang'];
    }
    else if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))
    {
        // load from web browser environment
        $hal = preg_split("/\;/", $_SERVER["HTTP_ACCEPT_LANGUAGE"]);
        $avail = preg_split('/\,/', array_shift($hal));
        if (!empty($avail))
        {
            // loop through languages
            $avail = array_map("strtolower", $avail);
            foreach ($avail as $inLang)
            {
                // check to see if language is available
                if (in_array($inLang, $GLOBALS['config']->languages))
                {
                    debug("global", "lang from browser: {$inLang}");
                    $lang = $inLang;
                    break;
                }

                // check to see if variation of language is available
                if (strlen($inLang) > 2)
                {
                    $inLangAlt = substr($inLang, 0, 2);
                    if (in_array($inLangAlt, $GLOBALS['config']->languages))
                    {
                        debug("global", "lang from browser: {$inLangAlt}");
                        $lang = $inLangAlt;
                        break;
                    }
                }
            }
        }
        unset($hal, $avail);
    }

    // return default language
    debug("global", "lang default: {$lang}");
    $html->lang = $lang;
    unset($lang);
}

// load global translation table
$html->load_trans("global");

?>

<!--VAR:[__layout|open]-->
<!--TITLE:[Run Windows applications on Linux, BSD, Solaris and macOS]-->
<!--META_KEYWORDS:[windows, linux, macintosh, solaris, freebsd]-->
<!--META_DESCRIPTION:[Open Source Software for running Windows applications on other operating systems.]-->
<!--META_OG:[type|website]-->

<div class="whq-page-container margin-bottom-md">

    <div class="row">
        <div class="col-md-7">

            <h1 class="title margin-bottom-sm">What is Wine?</h1>

            <p>Wine (originally an acronym for "Wine Is Not an Emulator") is a compatibility layer capable of running Windows applications on several POSIX-compliant operating systems, such as Linux, macOS, &amp; BSD. Instead of simulating internal Windows logic like a virtual machine or emulator, Wine translates Windows API calls into POSIX calls on-the-fly, eliminating the performance and memory penalties of other methods and allowing you to cleanly integrate Windows applications into your desktop.</p>

            <br>

            <h2 class="title margin-bottom-sm">Latest Releases</h2>
            <div class="row">
                <div class="col-sm-4 col-md-3">Stable:</div>
                <div class="col-sm-8">
                    <b><a href="{$root}/announce/{$config_stable_release}">Wine&nbsp;{$config_stable_release}</a></b>
                    <span class="small">
                      (<a href="https://gitlab.winehq.org/wine/wine/-/commits/wine-{$config_stable_release}">shortlog</a>)
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-md-3">Development:</div>
                <div class="col-sm-8">
                    <b><a href="{$root}/announce/{$config_master_release}">Wine&nbsp;{$config_master_release}</a></b>
                    <span class="small">
                        (<a href="//gitlab.winehq.org/wine/wine/-/commits/wine-{$config_master_release}">shortlog</a>)
                    </span>
                </div>
            </div>

            <br>

            <div id="carouselScreenshots" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/steam.jpg" alt="First slide">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/outlook2010.jpg" alt="Second slide">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/eveonline.jpg" alt="Third slide">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/photoshopcc2015.jpg" alt="Fourth slide">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/falloutnv.jpg" alt="Fifth slide">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/reason.jpg" alt="Sixth slide">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/skyrim_specialed.jpg" alt="Seventh slide">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/goggalaxy.jpg" alt="Eighth slide">
                    </div>
                    <div class="item active">
                        <img src="https://dl.winehq.org/share/images/screenshots/wizard101.jpg" alt="Ninth slide">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/matlab.jpg" alt="Tenth slide">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/sims3.jpg" alt="Eleventh slide">
                    </div>
                    <div class="item">
                        <img src="https://dl.winehq.org/share/images/screenshots/metatrader5.jpg" alt="Twelfth slide">
                    </div>
                </div>
                <a class="left carousel-control" href="#carouselScreenshots" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carouselScreenshots" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>
        <div class="col-md-5">

            <div class="winehq_menu">

                <a class="winehq_menu_item info" href="{$root}/about">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-info" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">About</span>
                    <span class="subtitle">Learn about the Wine project.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item dl" href="https://wiki.winehq.org/Download">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-download" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Download</span>
                    <span class="subtitle">Install the latest Wine.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item appdb" href="{$root}/news">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-newspaper" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">News</span>
                    <span class="subtitle">What is going on lately?</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item appdb" href="https://appdb.winehq.org">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-database" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Application Database</span>
                    <span class="subtitle">Does your application work with Wine?</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item help" href="{$root}/help">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-question" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Support</span>
                    <span class="subtitle">Get help using Wine.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item devel" href="{$root}/getinvolved">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-users" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Get Involved</span>
                    <span class="subtitle">Improve and develop Wine.</span>
                </a>
                <div class="clear"></div>

                <a class="winehq_menu_item donate" href="{$root}/donate">
                    <span class="winehq_badge fa-4x fa-layers fa-fw">
                        <i class="winehq_badge_inner fas fa-square"></i>
                        <i class="fa-inverse fas fa-donate" data-fa-transform="shrink-6"></i>
                    </span>
                    <span class="title">Donate</span>
                    <span class="subtitle">Donate to the Wine project.</span>
                </a>
                <div class="clear"></div>

            </div>

        </div>
    </div>
</div>

<div class="whq-page-container margin-top-md">

    <h2 class="title"><a href="{$root}/news">News and Updates</a></h2>

    <!--EXEC:[news?n=3]-->

    <p>
        <a href="{$root}/news" class="btn btn-default"><i class="fas fa-chevron-right"></i> News Archive</a>
        &nbsp;
        <a href="{$root}/wwn" class="btn btn-default"><i class="fas fa-newspaper"></i> World Wine News</a>
    </p>

</div>



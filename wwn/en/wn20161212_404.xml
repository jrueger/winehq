<?xml version="1.0" ?>
<kc>
<title>Wine Traffic</title>

<author contact="http://www.dawncrow.de/wine/">Andr&#233; Hentschel</author>
<issue num="404" date="12/12/2016" />
<intro> <p>This is the 404th issue of the World Wine News publication.
Its main goal is to inform you of what's going on around Wine. Wine is an open source implementation of the Windows API on top of X and Unix.  Think of it as a Windows compatibility layer.  Wine does not require Microsoft Windows, as it is a completely alternative implementation consisting of 100% Microsoft-free code, but it can optionally use native system DLLs if they are available.   You can find more info at <a href="http://www.winehq.org">www.winehq.org</a></p> </intro>
<stats posts="167" size="96" contrib="28" multiples="10" lastweek="17">

<person posts="9" size="15" who="hverbeet at gmail.com (Henri Verbeet)" />
<person posts="8" size="11" who="julliard at winehq.org (Alexandre Julliard)" />
<person posts="7" size="8" who="00cpxxx at gmail.com (Bruno Jesus)" />
<person posts="5" size="5" who="lauri.kentta at gmail.com (Lauri Kentt&#228;)" />
<person posts="3" size="3" who="stefandoesinger at gmail.com (Stefan D&#246;singer)" />
<person posts="2" size="2" who="jre.winesim at gmail.com (Jens Reyer)" />
<person posts="2" size="2" who="aeikum at codeweavers.com (Andrew Eikum)" />
<person posts="2" size="1" who="bunglehead at gmail.com (Nikolay Sivov)" />
<person posts="1" size="7" who="fgouget at codeweavers.com (Francois Gouget)" />
<person posts="1" size="2" who="alexhenrie24 at gmail.com (Alex Henrie)" />

</stats>

<section
	title="New Author"
	subject="WWN"
	archive=""
	posts="0"
>
<p>
    As with the last WWN, this time all the news are written by Stefan D&#246;singer! I just did the stats.
</p>
<p>
    Enjoy!
</p>
</section>

<section
	title="Code Freeze"
	subject="Code Freeze"
	archive="//www.winehq.org/pipermail/wine-devel/2016-December/115604.html"
	posts="1"
>
<![CDATA[
<p>
Alexandre has released Wine-2.0-rc1 and imposed martial^H^H^H^H^H^H^H code freeze:
</p>
<blockquote>
<pre>
The release of 2.0-rc1 marks the beginning of code freeze towards 2.0.
Most of you probably know the drill already, but from now on, only
targeted bug fixes and other "safe" changes, like test fixes,
translation updates, etc. will be accepted.

I know that many people will soon be out for the holidays, but I hope we
can still make significant progress with the open bugs, particularly
with the bugs from the regression list. Now would also be a good time to
retest your favorite applications to make sure that there are no
regressions that we have missed.

As usual, I'll be making release candidates every week, until we run out
of bugs...

Thank you all for helping make Wine 2.0 as good as we can!
</pre>
</blockquote>
]]>
</section>

<section
	title="Direct3D 11 development"
	subject="Direct3D"
	archive="//www.winehq.org/pipermail/wine-devel/2016-November/115230.html"
	posts="7"
>
<p>
    Wine‘s Direct3D 11 implementation is making progress! Over the past months Józef Kucia and Matteo Bruni have been slowly,
    but steadily piling on work in <a href="//source.winehq.org/git/wine.git/history/HEAD:/dlls/d3d11">d3d11.dll</a> and
    <a href="//source.winehq.org/git/wine.git/history/HEAD:/dlls/wined3d">wined3d.dll</a> to restructure and add the necessary features.
    A lot of work is still missing, so don‘t expect fancy titles to run yet.
    Simpler games have started working <a href="https://www.codeweavers.com/about/blogs/caron/2015/12/10/directx-11-really-james-didnt-lie">a year ago</a>.
</p>
<p>
    Last month Andrew Wesie sent a big patchset consisting of <a href="//www.winehq.org/pipermail/wine-patches/2016-November/155639.html">23 changes</a>
    to wine-patches that promised to make <a href="https://playoverwatch.com/">Blizzard‘s Overwatch</a> run on Wine.
    Not all of these changes concerned d3d11 – some were changes to the way certain CPU registers are handled in ntdll‘s exception handler.
    Andrew indicated that those changes are necessary to make the game‘s anti-debug protection work.
    The d3d-related patches add a few missing d3d11 pieces: Instancing-related shader registers, buffer shader resource views, primitive restart and so on.
    He also warned that these patches are not enough to make the game run, and more work was needed.
</p>
<p>
    Matteo and Henri Verbeet have provided <a href="//www.winehq.org/pipermail/wine-devel/2016-November/115230.html">reviews</a> for the patches.
    The main concern were missing tests and some implementation details.
    Andrew has submitted <a href="//source.winehq.org/git/wine.git/commit/ef8e2f2e75957c82ae2b4a4f5d0d23e9a69a3faf">tests</a> and
    an <a href="//source.winehq.org/git/wine.git/commit/c17462800a5372c7a7865d233d92f88ad9227dc4">implementation</a> for two sided stencil,
    tests for the <a href="//source.winehq.org/git/wine.git/commit/fe5b03beb5922f39ce1fa709984e4c2c98948061">primitive restart index</a> and
    the <a href="//source.winehq.org/git/wine.git/commit/809ad52ae6c4d762ca568487cf9c240af4f592bc">return instruction in shader functions</a>.
    These patches passed review and are already included in Wine git. Józef has cleaned up Andrew‘s implementation of the return instruction, which is upstream now too.
</p>
<p>
    Besides helping Andrew, Józef has been busy implementing Unordered Access Views (UAVs). They provide read-modify-write access to resources from shaders.
    Historically resources could either be read (Textures, Buffers) or written to (Render Targets), and it was not allowed to use the same resource for reading and writing at the same time.
    The OpenGL equivalent of this feature is called <a href="https://www.opengl.org/registry/specs/ARB/shader_image_load_store.txt">image load/store</a>.
</p>
<p>
    An open issue with anything newer than Direct3D9 is that wined3d still depends on legacy OpenGL 2 features and many drivers do not expose some features necessary for d3d10/11 in legacy contexts.
    With the <a href="//wiki.winehq.org/Useful_Registry_Keys">MaxVersionGL</a> key set wined3d will request a core context, but certain blitting corner cases are still broken.
    Mesa and the Nvidia binary driver mostly work. On MacOS you are most likely out of luck.
</p>
<p>
    And finally here is a screenshot provided by Andrew showing Overwatch running on his Linux desktop:
</p>
<img border="0" width="100%" src="//www.winehq.org/images/wwn404-overwatch.png"/>
<p>
    Again, the patches necessary to run the game are not in Wine or Wine-Staging yet, and they won’t make it in for Wine 2.0. So don’t hold your breath just yet!
</p>
</section>

<section
	title="Testbot"
	subject="Testbot"
	archive="//www.winehq.org/pipermail/wine-devel/2016-December/115516.html"
	posts="8"
>
<p>
    CodeWeavers is running a set of virtual Windows machines to run Wine‘s tests against various Windows versions.
    These VMs are available for Wine developers under <a href="//testbot.winehq.org/">testbot.winehq.org</a>.
    Francois Gouget is maintaining the Testbot and has posted the results of <a href="//www.winehq.org/pipermail/wine-devel/2016-December/115516.html">his recent update work</a>:
</p>
<p>
    The gist of the work was upgrading one of the host machines from Debian 7 to Debian 8.6, which brings in newer versions of QEMU and the Linux kernel.
    While this fixes a few known test failures, Francois is not happy with the performance of the upgraded machine yet.
    Memory writes went up mysteriously when restoring the virtual machines, slowing down the test process.
    Jonas Maebe and Henri Verbeet had some ideas what might be wrong, but at the time of the writing of this article the issue had not been solved yet.
    The updated host is back in active duty, but running only one virtual machine until the performance can be improved.
</p>
<p>
    Francois has also updated Windows 7 in the client VMs, which caused some test failures that Hans Leidekker has looked into and fixed.
</p>
</section>

<section
	title="WinMM Driver Messages"
	subject="winmm"
	archive="//www.winehq.org/pipermail/wine-devel/2016-December/115491.html"
	posts="9"
>
<p>
	Bruno Jesus tried to fix the old <a href="//bugs.winehq.org/show_bug.cgi?id=3930">bug 3930</a>: <a href="//www.winehq.org/pipermail/wine-patches/2016-December/156112.html">www.winehq.org/pipermail/wine-patches/2016-December/156112.html</a>.
	The bug is quite complicated: The affected games – there seem to be a few – uses the old <a href="https://en.wikipedia.org/wiki/Windows_legacy_audio_components#Multimedia_Extensions_.28MME.29">Windows Multimedia Extensions (winmm)</a> API to play sound.
	Winmm calls callback functions provided by the application.
	In one of these functions the games call <a href="https://msdn.microsoft.com/en-us/library/windows/desktop/ms686345(v=vs.85).aspx">SuspendThread()</a> and  put themselves to sleep indefinitely.
</p>
<p>
	This was working at the time the games were written because in Windows 9x winmm had a separate worker thread and called the callback functions from this thread.
	The callback put the main thread to sleep and would wake it up later, resuming operation. But in Wine and the Windows NT line winmm dispatches messages from the application‘s main thread.
	Once the winmm callback calls SuspendThread it won‘t be able to wake itself up any more.
</p>
<p>
	Our tests show that Wine behaves like modern Windows versions, and Bruno tried to reproduce the game‘s behavior on a modern Windows version, and sure enough,
	his test code died the same way the games do on Wine. He found the changelog of the sound middleware responsible for this behavior,
	and they <a href="//www.winehq.org/pipermail/wine-devel/2016-December/115491.html">changed it because it years ago because it was broken on Windows XP</a>. But yet, the games themselves are working fine on the same Windows installation.
</p>
<p>
	A discussion unfolded between Bruno and Andrew Eikum about how to proceed. Bruno‘s patch changed winmm‘s behavior depending on the Windows version set in winecfg.
	This would require a user to manually configure the Windows version to run these games – with the additional complication that a modern 64 bit WINEPREFIX will not allow you
	to set anything older than Windows XP – and having version specific code is always ugly. Bruno‘s tests show that Windows has a magic behavior switch for these games which either
	ignore the SuspendThread call or made winmm behave in a Windows 95 way.
</p>
<p>
	The issue is not yet resolved. Bruno was going to look deeper what triggered the different behavior on Windows and would try to replicate it automatically in Wine.
	It may require a more complicated shim layer for application compatibility similar to that on Windows or deeper changes in winmm.
	Either of those involves a lot of work and risk of regressions, so he decided to wait until after the upcoming stable release.
</p>
</section>

<section
	title="COM Port Sniffing"
	subject="serial"
	archive="//www.winehq.org/pipermail/wine-devel/2016-December/115516.html"
	posts="6"
>
<p>
	Bill Lionheart wrote to wine-devel <a href="//www.winehq.org/pipermail/wine-devel/2016-November/115454.html">about a different kind of problem</a>:
	He tried to use an amateur radio GPRS tracker with its Windows software and intercept the communication between software and device.
</p>
<p>
	The device shows up on Linux as a USB to serial device - /dev/ttyUSBx. He created a symlink to ~/.wine/dosdevices/com0:,
	and luckily the Windows software is able to talk to the device and it functions properly. But using the software was not his actual goal.
</p>
<p>
	He unsuccessfully tried Linux tools to sniff serial port traffic and various Windows tools. None of them were able to read the traffic.
	Christopher Harrington came asked a few questions to make sure Bill‘s setup was correct, which it was.
</p>
<p>
	Henri Verbeet asked for more details about the tools Bill tried: <a href="//www.winehq.org/pipermail/wine-devel/2016-December/115521.html">socat and wireshark</a> on the Linux side.
	They saw outgoing communication, but stopped replies from the device from getting back to the Windows app.
	But by luck he ran across a tutorial that used a tool called tshark to analyze the USB traffic, which luckily worked.
	This left Henri a bit puzzled because tshark and wireshark work essentially in the same way, through the usbmon module.
</p>
<p>
	The bottom line is that Bill achieved what he wanted. We‘re all hoping that the records of his endeavor help someone in the future.
</p>
</section>

<section
    title="Weekly AppDB/Bugzilla Status Changes"
    subject="AppDB/Bugzilla"
    archive="//appdb.winehq.org"
    posts="0"
>
<topic>AppDB / Bugzilla</topic>
<center><b>Bugzilla Changes:</b></center>
<p><center>
<table border="1" bordercolor="#222222" cellspacing="0" cellpadding="3">
  <tr><td align="center">
        <b>Category</b>
  </td><td>
         <b>Total Bugs Last Issue</b>
  </td><td>
        <b>Total Bugs This Issue</b>
  </td><td>
        <b>Net Change</b>
  </td></tr>  <tr>
    <td align="center">
     UNCONFIRMED
    </td>
    <td align="center">
     2897
    </td>
    <td align="center">
     2876
    </td>
    <td align="center">
      -21
    </td>
  </tr>  <tr>
    <td align="center">
     NEW
    </td>
    <td align="center">
     3053
    </td>
    <td align="center">
     3057
    </td>
    <td align="center">
      +4
    </td>
  </tr>  <tr>
    <td align="center">
     ASSIGNED
    </td>
    <td align="center">
     21
    </td>
    <td align="center">
     21
    </td>
    <td align="center">
      0
    </td>
  </tr>  <tr>
    <td align="center">
     STAGED
    </td>
    <td align="center">
     205
    </td>
    <td align="center">
     204
    </td>
    <td align="center">
      -1
    </td>
  </tr>  <tr>
    <td align="center">
     REOPENED
    </td>
    <td align="center">
     122
    </td>
    <td align="center">
     122
    </td>
    <td align="center">
      0
    </td>
  </tr>  <tr>
    <td align="center">
     NEEDINFO
    </td>
    <td align="center">
     153
    </td>
    <td align="center">
     156
    </td>
    <td align="center">
      +3
    </td>
  </tr>  <tr>
    <td align="center">
     RESOLVED
    </td>
    <td align="center">
     302
    </td>
    <td align="center">
     344
    </td>
    <td align="center">
      +42
    </td>
  </tr>  <tr>
    <td align="center">
     CLOSED
    </td>
    <td align="center">
     35146
    </td>
    <td align="center">
     35179
    </td>
    <td align="center">
      +33
    </td>
  </tr>   <tr><td align="center">
      TOTAL OPEN
   </td><td align="center">
      6176
   </td><td align="center">
      6158
   </td><td align="center">
      -18
   </td></tr>
   <tr><td align="center">
       TOTAL
   </td><td align="center">
      41899
   </td><td align="center">
      41959
   </td><td align="center">
      +60
   </td></tr>
</table>
</center></p>
<br /><br />
<center><b>AppDB Application Status Changes</b></center>
<p><i>*Disclaimer: These lists of changes are automatically  generated by information entered into the AppDB.
These results are subject to the opinions of the users submitting application reviews.
The Wine community does not guarantee that even though an application may be upgraded to 'Gold' or 'Platinum' in this list, that you
will have the same experience and would provide a similar rating.</i></p>
<div align="center">
   <b><u>Updates by App Maintainers</u></b><br /><br />
    <table width="80%" border="1" bordercolor="#222222" cellspacing="0" cellpadding="3">
      <tr>
        <td><b>Application</b></td>
        <td width="140"><b>Old Status/Version</b></td>
        <td width="140"><b>New Status/Version</b></td>
        <td width="20" align="center"><b>Change</b></td>
      </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32907">Battlefield 2142 1.51</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.55)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.23)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=18218">Borderlands Steam</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.9.2)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.20)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=13562">Silent Hunter 4 1.5</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.5.22)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.8.5)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31410">VUDUToGo 2.x</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.33)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (2.0-rc1)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=27970">Defiance NA Retail</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.11)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.9.23)
             </td><td align="center">
                <div style="color: #990000;">-3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32923">Altium Designer 16</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.9.0)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.9.23)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=17301">Guitar Hero World Tour 1.x</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.3.15)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.9.23)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=33024">Saturn PCB Design Toolkit 6.7+</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.8-rc3)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.9.23)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td colspan="3">
                Total Change
             </td><td align="center">
               <div style="color: #990000;">-3</div>
             </td>
           </tr>
        </table>  <br />   <b><u> Updates by the Public </u></b> <br /><br />
   <table width="80%" border="1" bordercolor="#222222" cellspacing="0" cellpadding="3">
      <tr>
        <td><b>Application</b></td>
        <td width="140"><b>Old Status/Version</b></td>
        <td width="140"><b>New Status/Version</b></td>
        <td width="20"><b>Change</b></td>
      </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=34535">EAC - Exact Audio Copy 1.3</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.6.2)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.24)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=1230">Grand Theft Auto III 1.1</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.22)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.21)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=24425">1Heroes of Might and Magic III Complete (gog.com v...</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.6.2)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.23)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=29646">Heroes of Might and Magic III Horn of the Abyss</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.8.3)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.24)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=17020">LEGO Indiana Jones: The Original Adventures 1.0</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.5.16)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.24)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32165">LEGO Star Wars II: The Original Trilogy 1.0</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.38)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.24)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=12828">LEGO Star Wars: The Video Game 1.0</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.4.1)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.24)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=7071">Netscape Communicator 4.8</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.3.27)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.8.5)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31354">Neverwinter Nights Diamond Edition (GoG)</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.8.5)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.23)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=34056">No Man's Sky GOG</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.9.16)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.24)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=33650">Railroad Tycoon 3 1.05-gog-6</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.6.2)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.24)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=25711">Sins of a Solar Empire Rebellion</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.19)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.8.5)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=25911">Adobe Audition CS6 (5.0)</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.9.17)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.9.23)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31296">2Buzz Aldrin's Space Program Manager 1.1 (Boxed Co...</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.28)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.8.5)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32580">Final Fantasy XIV Heavensward (Official Client)</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.9.11)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.9.23)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=6533">Magic: The Gathering - Battlegrounds 1.4</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.5.18)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.9.24)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=33774">Sayonara Umihara Kawase Steam</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.9.11)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.9.24)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=19398">Caesar III GoG.com version (1.0.1.0)</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.8.1)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.9.21)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=10107">Crysis Crysis 1.x</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.9.11)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.9.23)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=2344">Desperados: Wanted Dead or Alive 1.x</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.9.24)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=33881">Family Tree Builder 8.x</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.8.3)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.9.24)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=34428">Sonic Utopia Demo v1</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.6.2)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.9.23)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=34445">Europa Universalis IV Non-Steam "Offline"</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.9.23)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.9.24)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=25447">Silver Chaos 1 1.2</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.3.37)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.9.23)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31322">iTunes 12.x</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.9.21)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.9.24)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=21829">LEGO Digital Designer 4.x</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.6.2)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (2.0-rc1)
             </td><td align="center">
                <div style="color: #990000;">-4</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=34269">Paladins: Champions of the realm .33</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.9.18)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.9.23)
             </td><td align="center">
                <div style="color: #990000;">-3</div>
             </td>
           </tr>           <tr>
             <td colspan="3">
                Total Change
             </td><td align="center">
               <div style="color: #000000;">+11</div>
             </td>
           </tr>
        </table></div>
</section></kc>

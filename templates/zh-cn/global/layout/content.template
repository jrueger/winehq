<!doctype html>
<html lang="{$html_lang}">
<!--
 __          ___            _    _  ____
 \ \        / (_)          | |  | |/ __ \
  \ \  /\  / / _ _ __   ___| |__| | |  | |
   \ \/  \/ / | | '_ \ / _ \  __  | |  | |
    \  /\  /  | | | | |  __/ |  | | |__| |
     \/  \/   |_|_| |_|\___|_|  |_|\___\_\
-->
<head>
    <title>{$page_title}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="SearchTitle" content="{$page_title}">
    <meta name="copyright" content="Copyright {$curtime_year}. WineHQ.org All rights reserved">

    {$meta_keywords}
    {$meta_description}
    {$meta_og}
    {$rel_canonical}

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    {$css_links}

    <script src="https://code.jquery.com/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
    {$js_links}

    <link rel="shortcut icon" type="image/png" href="https://dl.winehq.org/share/images/winehq_logo_32.png">
    {$rss_link}
</head>
<body class="page-{$page_name}{$page_body_class}">

<div id="whq-tabs">
    <div class="whq-tabs-menu">&#9776;</div>
    <ul>
        <li class="s"><a href="{$base_url}">{trans($tab_winehq)}</a></li>
        <li><a href="https://wiki.winehq.org/">{trans($tab_wiki)}</a></li>
        <li><a href="//appdb.winehq.org/">{trans($tab_appdb)}</a></li>
        <li><a href="//bugs.winehq.org/">{trans($tab_bugs)}</a></li>
        <li><a href="//forums.winehq.org/">{trans($tab_forums)}</a></li>
    </ul>
</div>
<div class="clear"></div>

<div id="whq-logo-glass"><a href="{$root}/"><img src="https://dl.winehq.org/share/images/winehq_logo_glass.png" alt=""></a></div>
<div id="whq-logo-text"><a href="{$root}/"><img src="https://dl.winehq.org/share/images/winehq_logo_text.png" alt="WineHQ" title="WineHQ"></a></div>

<div id="whq-search-box">
    <form action="{$root}/search" id="cse-search-box">
        <div class="input-group input-group-sm">
            <span class="input-group-addon"><i class="fas fa-search"></i></span>
            <input type="text" name="q" value="" class="form-control" placeholder="{trans($search_placeholder)}">
        </div>
    </form>
</div>

<div id="whq-page-body" class="container-fluid">
    {$page_body}
</div>

<div id="whq-footer" class="container-fluid">
    <div class="row">
        <div class="col-xs-6">
            <i class="fas fa-language fa-2x" data-fa-transform="down-3"></i> {$langCur} (<a href="{$root}/lang">{$langChange}</a>)
        </div>
        <div class="col-xs-6 text-right">
            <a href="https://wiki.winehq.org/WineHQ_Wiki:Privacy_policy">隐私政策</a><br>
	    如果您有关于隐私调查相关的问题，请与 <a href="mailto:privacy@winehq.org">privacy@winehq.org</a> 联系<br><br>
            {trans($hosted_by)}
            <a href="https://www.codeweavers.com/"><img src="https://dl.winehq.org/share/images/cw_logo_sm.png" alt="CodeWeavers"
            title="{trans($hosted_by_alt)}"></a>
        </div>
    </div>
</div>

<!-- DEBUG -->
{$debug_log}
<!-- END DEBUG -->

</body>
</html>

<!--TITLE:[실시간 커뮤니티 채팅]-->

<h1 class="title">실시간 커뮤니티 채팅</h1>

<p><a href="https://libera.chat/">Libera.​Chat</a>에서 여러 Wine IRC 채널을
호스트하고 있습니다. <a href="https://hexchat.github.io/">HexChat</a>과 같은 IRC
프로그램으로 채팅에 참여할 수 있습니다. 다음 설정을 사용하십시오:</p>

<blockquote>
    <b>서버:</b> irc.libera.chat<br>
    <b>포트:</b> 6697<br>
</blockquote>

<p>대화하고 싶은 주제에 따라서 채널을 선택하십시오:</p>

<blockquote>
    <b>#winehq:</b> Wine 실행에 대한 사용자 지원과 도움<br>
    <b>#crossover:</b> CrossOver 실행에 대한 사용자 지원과 도움<br>
    <b>#winehackers:</b> Wine 개발과 기타 기여<br>
    <b>#winehq-social:</b> 다른 커뮤니티 구성원과의 일반적인 대화<br>
</blockquote>

<p>Firefox 및 IRC URL을 지원하는 브라우저를 사용 중이라면 다음 링크를 클릭하여
접속할 수 있습니다:</p>
<ul>
    <li><a href="irc://irc.libera.chat/winehq">#winehq</a></li>
    <li><a href="irc://irc.libera.chat/crossover">#crossover</a></li>
    <li><a href="irc://irc.libera.chat/winehackers">#winehackers</a></li>
    <li><a href="irc://irc.libera.chat/winehq-social">#winehq-social</a></li>
</ul>

<p>대화가 주제를 벗어나지 않고 최대한 건설적으로 진행될 수 있도록 IRC에 물어보기
전에 질문할 것에 대해서 찾아보는 것을 추천합니다. <a
href="https://wiki.winehq.org/FAQ">Wine FAQ</a>, <a
href="//appdb.winehq.org">AppDB</a>, <a
href="//www.winehq.org/download">다운로드 페이지</a>에 있는 내용부터 검색을
시작해 보십시오.</p>

<h2>IRC 규칙과 제재</h2>

<p>다른 사람에게 공격적으로 나서거나 무시하지 않는 것 이외에도 IRC 채널에
참여하는 모든 사람이 지켜야 할 규칙이 있습니다. 규칙을 첫 번째로 지키지 않았다면
대개의 경우 실수로 보고 경고만 받게 됩니다. 여러 번 경고를 받게 된다면 채널에서
추방당할 것입니다.</p>

<p>여러 번 추방당한 이후에도 계속 규칙을 어긴다면 최대 2시간까지 채널 접속이
차단될 수 있습니다. 이 이후에도 규칙을 어긴다면 무기한 차단되며, 차단 해제
요청을 하지 않는 한 계속 유지됩니다. 차단에 이의를 제기하고 싶으면
<b>#winehq-social</b> 채널(만약 <b>#winehq-social</b> 채널에서 차단당했으면
<a href="mailto:wine-devel@winehq.org">wine-devel 메일링 리스트</a>)에 자신이 왜
차단당했으며 왜 차단이 해제되어야 하는지에 대한 메시지를 남겨 주십시오.</p>

<table class="table">
<thead>
    <tr class="black inverse small">
        <th>규칙</th>
        <th>설명</th>
        <th>경고</th>
        <th>추방</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td>스팸 전송 금지</td>
        <td></td>
        <td>1</td>
        <td>2</td>
    </tr>
    <tr>
        <td>1, 2줄 이상의 내용을 붙여넣을 때에는 Pastebin 사용</td>
        <td><a href="http://winehq.pastebin.com">winehq.pastebin.com</a> 및
            <a href="http://pastebin.ca">pastebin.ca</a> 등을 사용할 수 있습니다.</td>
        <td>0</td>
        <td>5</td>
    </tr>
    <tr>
        <td>채널 주제에 맞는 이야기 하기</td>
        <td>확실하지 않은 경우 <b>#winehq</b> 채널에 참여할 채널을 물어
        보십시오.</td>
        <td>2</td>
        <td>3</td>
    </tr>
    <tr>
        <td>Wine과 CrossOver만 해당하는 채널에서 지원함</td>
        <td>Sidenet, WineDoors, Cedega, IEs4Linux 등은 지원되지 <b>않습니다</b>.</td>
        <td>2</td>
        <td>1</td>
    </tr>
    <tr>
        <td><b>#winehq</b>에 질문하기 전에 최신 버전의 Wine 사용 확인</td>
        <td>확실하지 않은 경우 명령행에 <tt>wine --version</tt> 명령을 실행하여
        사용 중인 버전을 알아 보십시오.</td>
        <td>3</td>
        <td>1</td>
    </tr>
    <tr>
        <td>도움을 받고 싶다면 차례 기다리기</td>
        <td></td>
        <td>3</td>
        <td>1</td>
    </tr>
    <tr>
        <td>불법 복제 소프트웨어에 관한 언급 <b>금지</b></td>
        <td></td>
        <td>1</td>
        <td>1</td>
    </tr>
</tbody>
</table>

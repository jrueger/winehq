<!--TITLE:[Live Wine Chat]-->

<h1 class="title">Live Wine Chat</h1>

<p><a href="https://libera.chat/">Libera.​Chat</a> heeft verschillende IRC
kanalen over Wine. Gebruik uw favorite IRC programma met de volgende instellingen:</p>

<blockquote>
    <table>
    <tr>
        <td style="width:150px"><b>Server:</b></td>
        <td>irc.libera.chat</td>
    </tr>
    <tr>
        <td><b>Poort:</b></td>
        <td>6697</td>
    </tr>
    <tr>
        <td><b>TLS:</b></td>
        <td>ingeschakeld</td>
    </tr>
    </table>
</blockquote>

<p>Afhankelijk van het onderwerp kies één van de volgende kanalen:</p>

<blockquote>
    <table>
    <tr>
        <td style="width:150px"><b>#winehq:</b></td>
        <td>Ondersteuning en hulp met Wine voor gebruikers</td>
    </tr>
    <tr>
        <td><b>#crossover:</b></td>
        <td>Ondersteuning en hulp met CrossOver voor gebruikers</td>
    </tr>
    <tr>
        <td><b>#winehackers:</b></td>
        <td>Ontwikkeling en bijdragen aan Wine</td>
    </tr>
    <tr>
        <td><b>#winehq-social:</b></td>
        <td>Kletsen met andere Wine gebruikers en ontwikkelaars</td>
    </tr>
    </table>
</blockquote>

<p>Mensen die Firefox gebruiken of een andere webbrowser die IRC internetadressen ondersteunt,
kunnen de chat bezoeken met:</p>
<ul>
    <li><a href="irc://irc.libera.chat/winehq">#winehq</a></li>
    <li><a href="irc://irc.libera.chat/crossover">#crossover</a></li>
    <li><a href="irc://irc.libera.chat/winehackers">#winehackers</a></li>
    <li><a href="irc://irc.libera.chat/winehq-social">#winehq-social</a></li>
</ul>

<p>Om de gespekken zo nuttig mogelijk te houden, doorzoek eerst de
<a href="https://wiki.winehq.org/FAQ_nl">Wine FAQ</a>,
<a href="//appdb.winehq.org">AppDB</a> en de
<a href="https://wiki.winehq.org/Download_nl">download pagina's</a> voordat
u een vraag stelt.</p>

<h2>IRC Regels en Straffen</h2>

<p>Naast niet onnodig beledigend en brutaal zijn, zijn er een paar regels
waaraan iedereen op IRC zich aan moet houden. In de meeste gevallen wordt
een overtreding van de regels de eerste keer als een vergissing gezien met
een waarschuwing als gevolg. Na meerdere overtredingen wordt u uit het
IRC-kanaal gezet.</p>

<p>Als u de regels blijft overtreden, nadat u uit een kanaal bent gezet, wordt
u de toegang voor twee uur tot het kanaal ontzegt. De volgende sanctie is
complete ontzegging tot het kanaal. Om een ontzegging ongedaan te maken ga
naar <b>#winehq-social</b> (of mail naar de
<a href="mailto:wine-devel@winehq.org">wine-devel mailinglijst</a> als u uit
<b>#winehq-social</b> bent gezet) en leg uit waarom u uit het kanaal bent
gezet en waarom de sanctie moet worden opgeheven.</p>

<table class="table">
<thead>
    <tr class="black inverse small">
        <th>Regel</th>
        <th>Uitleg</th>
        <th>Waarschuwingen</th>
        <th>Max. Overtredingen</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td>Geen spam.</td>
        <td></td>
        <td>1</td>
        <td>2</td>
    </tr>
    <tr>
        <td>Gebruik pastebin als u meer dan 1 of 2 regels tekst plakt</td>
        <td></td>
        <td>0</td>
        <td>5</td>
    </tr>
    <tr>
        <td>Gebruik het goede kanaal</td>
        <td>Bij twijfel, vraag het in <b>#winehq</b>.</td>
        <td>2</td>
        <td>3</td>
    </tr>
    <tr>
        <td>Alleen Wine en CrossOver worden ondersteund</td>
        <td>PlayOnLinux, PlayOnMac enz. worden <b>niet</b>
            ondersteund.</td>
        <td>2</td>
        <td>1</td>
    </tr>
    <tr>
        <td>Voordat u hulp vraagt in <b>#winehq</b>, zorg ervoor dat u de laatste
        versie van Wine heeft.</td>
        <td>U kunt met <tt>wine --version</tt> uw versie vinden</td>
        <td>3</td>
        <td>1</td>
    </tr>
    <tr>
        <td>Wacht op uw beurt.</td>
        <td></td>
        <td>3</td>
        <td>1</td>
    </tr>
    <tr>
        <td>Bespreek <b>geen</b> illegale stoftware.</td>
        <td></td>
        <td>1</td>
        <td>1</td>
    </tr>
</tbody>
</table>

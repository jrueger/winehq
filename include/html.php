<?php

/*
  WineHQ
  HTML class
  by Jeremy Newman <jnewman@codeweavers.com>
*/

class html
{
    public $lang = "en";                          // default language to use for templates and translation
    public $_file_root;                           // root of website on filesystem
    public $_web_root;                            // root of website on URL
    public $page;                                 // current base PAGE template being loaded
    public $cli = false;                          // command line mode, changes base_url() handling
    public $force_title = false;                  // force title to page_title (in site)
    public $page_title;                           // current page title
    public $meta_keywords;                        // meta keywords to load in showpage()
    public $meta_description;                     // meta desc to load in showpage()
    public $meta_og = array();                    // meta open graph array in showpage()
    public $rss_link;                             // set RSS link in showpage()
    public $_rel_canonical;                       // set canonical link in header in showpage()
    public $_error_mode = false;                  // enable error mode
    public $_view_mode = "default";               // view mode, changes output in showpage()
    public $relative = true;                      // relative mode in get_root()
    public $in403 = false;                        // enable 403 header in showpage
    public $in404 = false;                        // enable 404 header in showpage
    private $_navbar = "";                        // additional navbar items
    private $template_cache = array();            // cache for template data
    private $template_vars = array();             // vars to pass to EXEC script from template
    private $trans_table = array();               // translation table data
    private $last_modified = 0;                   // default last modified for header

    // header code in showpage()
    private $header_code = array('pre' => '', 'post' => '');

    // default CSS links to add in header in showpage()
    private $css_links = array('styles');

    // default JS links to add in header in showpage()
    private $js_links = array('utils');

    // constructor
    function __construct ($file_root = '.')
    {
        // set the file root
        $this->_file_root = $file_root;
        $this->_web_root = $GLOBALS['config']->base_root;

        // detect CLI (command line) mode
        if (empty($_SERVER['HTTP_HOST']))
            $this->cli = true;
    }

    // SHUTDOWN HANDLER (FATAL PHP errors)
    public function error_shutdown ()
    {
        // get information about last error
        $error = error_get_last();
        // handle error if defined
        if (!empty($error))
        {
            // output error
            $this->error_handler($error['type'], $error['message'], $error['file'], $error['line']);
        }
    }

    // ERROR_HANDLER
    public function error_handler ($errno, $errstr, $errfile, $errline, $errcontext = null)
    {
        global $config;

        // debug text
        $debug = "";

        // PHP error codes
        $err = array(
                     1    => 'E_ERROR',
                     2    => 'E_WARNING',
                     4    => 'E_PARSE',
                     8    => 'E_NOTICE',
                     16   => 'E_CORE_ERROR',
                     32   => 'E_CORE_WARNING',
                     64   => 'E_COMPILE_ERROR',
                     128  => 'E_COMPILE_WARNING',
                     256  => 'E_USER_ERROR',
                     512  => 'E_USER_WARNING',
                     1024 => 'E_USER_NOTICE',
                     2047 => 'E_ALL',
                     2048 => 'E_STRICT',
                     4096 => 'E_RECOVERABLE_ERROR',
                     8192 => 'E_DEPRECATED'
                    );

        // clean up errfile
        $errfile = basename($errfile);

        // log PHP non-fatal code errors
        if ($GLOBALS['config']->web_debug and
            ($errno == E_NOTICE or $errno == E_USER_NOTICE or $errno == E_USER_WARNING or $errno == E_STRICT or $errno == E_DEPRECATED))
            debug("error", "ERROR:[{$err[$errno]}] {$errfile}:{$errline} - {$errstr}");

        // do not fail on PHP STRICT or DEPRECIATED code on production site
        if ($errno and ($errno == E_STRICT or $errno == E_DEPRECATED) and !$config->test_mode)
            return;

        // don't exit on a notice or warning
        if ($errno == E_NOTICE or $errno == E_WARNING)
            return;

        // write to the error log (move above the above return to get NOTICE and WARNING messages)
        if (isset($config->error_log) and file_exists($config->error_log))
        {
            error_log(
                      "[".date("D M j G:i:s Y",time())."] [".$err[$errno]."] ".$errfile.":".$errline." - ".$errstr."\n",
                      3,
                      $config->error_log
                     );
        }

        // don't exit on a USER notice or warning, but log above
        if ($errno == E_USER_NOTICE or $errno == E_USER_WARNING)
            return;

        // show additional debug output
        if ($config->web_debug and !empty($errcontext) and function_exists('debug_backtrace'))
        {
            // build context
            $ctx = '';
            foreach ($errcontext as $key => $value)
            {
                switch (gettype($value))
                {
                    case "string":
                        $ctx .= "[$key] => $value\n";
                        break;
                    default:
                        $ctx .= "[$key] => ".gettype($value)."\n";
                }
            }

            // build backtrace
            $backtrace = debug_backtrace();
            $output = "";
            foreach ($backtrace as $bt)
            {
               $args = '';
               if (isset($bt['args']))
               {
                   foreach ($bt['args'] as $a)
                   {
                       if (!empty($args))
                           $args .= ', ';
                       switch (gettype($a))
                       {
                           case 'integer':
                           case 'double':
                               $args .= $a;
                               break;
                           case 'string':
                               $a = $this->encode(substr($a, 0, 64)).((strlen($a) > 64) ? '...' : '');
                               $args .= "\"$a\"";
                               break;
                           case 'array':
                               $args .= 'Array('.count($a).')';
                               break;
                           case 'object':
                               $args .= 'Object('.get_class($a).')';
                               break;
                           case 'resource':
                               $args .= 'Resource('.strstr($a, '#').')';
                               break;
                           case 'boolean':
                               $args .= $a ? 'True' : 'False';
                               break;
                           case 'NULL':
                               $args .= 'Null';
                               break;
                           default:
                               $args .= 'Unknown';
                       }
                   }
               }
               $output .= "\n";
               if (isset($bt['file']))
                   $output .= "file: {$bt['file']}\n";
               if (isset($bt['line']))
                   $output .= "line: {$bt['line']}\n";
               if (isset($bt['class']))
                   $output .= "call: {$bt['class']}{$bt['type']}{$bt['function']}($args)\n";
               else if (isset($bt['type']))
                   $output .= "call: {$bt['type']}{$bt['function']}($args)\n";
               else
                   $output .= "call: {$bt['function']}($args)\n";
            }

            $debug = "Context: \n".$ctx."\n\n";
            $debug .= "Backtrace: \n".$output."\n";
        }

        // display error page and exit
        $ERROR_vars = array(
                            'errno'      => $err[$errno],
                            'errstr'     => $errstr,
                            'errfile'    => $errfile,
                            'errline'    => $errline,
                            'debug'      => $debug
                           );
        $this->_error_mode = 1;
        $this->clear_buffer();
        $PAGE_vars = array(
                          'page_title'       => "{$config->site_name} - Error - {$this->page_title}",
                          'css_links'        => $this->get_header_links('css'),
                          'js_links'         => $this->get_header_links('js'),
                          'page_body'        => $this->template("local", 'global/fatal_error', $ERROR_vars),
                          'copyright_year'   => date("Y", time()),
                          'userdata'         => (method_exists($GLOBALS['login'], 'getUserData') ? base64_encode(json_encode($GLOBALS['login']->getUserData())) : ''),
                          'debug_log'        => ($config->web_debug ? $this->template("local", "global/layout/debug_log",
                                                    array("debug_log" => $this->encode($GLOBALS['debug_log']))) : '')
                         );
        header("HTTP/1.1 500 Internal Server Error");
        $this->http_header("text/html");
        echo $this->template("local", "global/layout/content_nonav", $PAGE_vars);
        exit('fatal error!');
    }

    // GET ROOT - get the root of the website (relative aware), with fallback to full url
    public function get_root ()
    {
        // command line mode: always returns full url
        if ($this->cli)
            return $this->base_url();

        // relative mode: use current web root
        if ($this->relative)
            return $this->_web_root;

        // fall back to base_url forcing full url
        return $this->base_url();
    }

    // BASE URL - return the full URL for the web root (no trailing slash like _web_root)
    public function base_url ($lang = false)
    {
        if ($this->cli or $lang)
        {
            // use default lang if not defined
            if (empty($lang) or empty($GLOBALS['config']->web_domain[$lang]))
                $lang = $GLOBALS['config']->lang;

            // build URL from language options
            $url_domain = "{$GLOBALS['config']->web_domain[$lang]}";
        }
        else
        {
            // when on the website and language not overridden, use the currently defined host
            $url_domain = "{$_SERVER['HTTP_HOST']}";
        }

        // return constructed URL
        if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')
            return "https://{$url_domain}{$this->_web_root}";
        else
            return "http://{$url_domain}{$this->_web_root}";
    }

    // clean and decode user input, never trust user input
    public function clean_input ($var, $strip_tags = false)
    {
        if (is_array($var))
        {
            // loop through nested vars
            $cVar = array();
            foreach ($var as $key => $val)
            {
                // die if key starts with < or >, that will never happen
                if (preg_match('/^(\<|\>|&gt;|&lt;|%3C|%3E)/', $key))
                    trigger_error('Invalid Request!', E_USER_ERROR);
                // we clean both the keys and vars, also if the key contains special chars, it is dropped
                $cKey = $this->clean_input($key, $strip_tags);
                if (preg_match('/[a-z0-9-_\.\[\]]+/i', $cKey))
                    $cVar[$cKey] = $this->clean_input($val, $strip_tags);
            }
            $var = $cVar;
        }
        else
        {
            // strip javascript (cross site scripting attempts)
            $var = $this->strip_js($var);
            // remove dangerous internal template tags
            $var = $this->template_strip($var);
            // remove HTML
            if ($strip_tags)
                $var = strip_tags($var);
        }
        return $var;
    }

    // strip javascript from string or URL
    public function strip_js ($val)
    {
        // straight replacements, the user should never need these since they're normal characters
        // this prevents like <IMG SRC=&#X40&#X61&#X76&#X61&#X73&#X63&#X72&#X69&#X70&#X74&#X3A
        $search = 'abcdefghijklmnopqrstuvwxyz';
        $search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $search .= '1234567890!@#$%^&*()';
        $search .= '~`",;:?+/={}[]-_|\'\\';
        for ($i = 0; $i < strlen($search); $i++)
        {
            // ;? matches the ;, which is optional
            // 0{0,7} matches any padded zeros, which are optional and go up to 8 chars
            // &#x0040 @ search for the hex values
            $val = preg_replace('/(&#[xX]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $val); // with a ;
            // &#00064 @ 0{0,7} matches '0' zero to seven times
            $val = preg_replace('/(&#0{0,8}'.ord($search[$i]).';?)/', $search[$i], $val); // with a ;
        }
        // remove javascript attempts (we do not allow the users to input js, in any form anywhere on the site)
        $val = preg_replace('/(\<|%3C|\<%00)script.*(\>|%3E).*(\<|%3C)(\/|%2F)script.*(\>|%3E|%00\>)/i', '', $val);
        $val = preg_replace('/("|%22)javascript(:|%3A).*("|%22)/i', '""', $val);
        $val = preg_replace('/(href|src|onclick|onload)(=|%3D)javascript(:|%3A)(.*)\(.*\)/i', '$1$2', $val);
        // return cleaned URL
        return $val;
    }

    // ADD HEADER LINK -- add a link to the page headers
    public function add_header_link ($link, $type = 'js')
    {
        switch ($type)
        {
            case "css":
                if (!in_array($link, $this->css_links))
                    $this->css_links[] = $link;
                break;
            case "js":
                if (!in_array($link, $this->js_links))
                    $this->js_links[] = $link;
                break;
            case "pre":
            case "post":
                    $this->header_code[$type] .= $link;
                    $link = "CODE";
                break;
        }
        debug("template", "loading header link: {$type}:[{$link}]");
    }

    // GET HEADER LINKS
    public function get_header_links ($type = 'js')
    {
        // css links
        if ($type == 'css' and count($this->css_links))
        {
            $css_links = "";
            if ($GLOBALS['config']->use_mirror and $GLOBALS['config']->mirror_root)
                $cssroot = $GLOBALS['config']->mirror_root;
            else
                $cssroot = $this->base_url();
            $i = 0;
            foreach ($this->css_links as $css_link)
            {
                $css_file = "{$this->_file_root}/css/{$css_link}.css";
                if (file_exists($css_file))
                {
                    $mtime = filemtime($css_file);
                    if ($css_link == "print")
                    {
                        $css_links .= ($i > 0 ? str_repeat(" ", 4) : '').
                                      "<link rel=\"stylesheet\" href=\"{$cssroot}/css/{$css_link}.css?v={$mtime}\" type=\"text/css\" media=\"print\">\n";
                    }
                    else
                    {
                        $css_links .= ($i > 0 ? str_repeat(" ", 4) : '').
                                      "<link rel=\"stylesheet\" href=\"{$cssroot}/css/{$css_link}.css?v={$mtime}\" type=\"text/css\" media=\"screen, print\">\n";
                    }
                    $i++;
                    unset($mtime);
                }
                unset($css_file);
            }
            unset($i,$cssroot);
            return $css_links;
        }
        // javascript links
        if ($type == 'js' and count($this->js_links))
        {
            $js_links = "";
            // base URL for internal scripts
            if ($GLOBALS['config']->use_mirror and $GLOBALS['config']->mirror_root)
                $jsroot = $GLOBALS['config']->mirror_root;
            else
                $jsroot = $this->base_url();
            // loop counter
            $i = 0;
            // loop js links
            foreach ($this->js_links as $js_link)
            {
                // load internal javascript
                $js_file = "{$this->_file_root}/js/{$js_link}.js";
                if (file_exists($js_file))
                {
                    $mtime = filemtime($js_file);
                    $extra = '';
                    if ($js_link == 'utils')
                        $extra = "&amp;web_root={$this->_web_root}";
                    $js_links .= ($i > 0 ? str_repeat(" ", 4) : "").
                                "<script src=\"{$jsroot}/js/{$js_link}.js?v={$mtime}{$extra}\" type=\"text/javascript\"></script>\n";
                    $i++;
                    unset($mtime, $extra);
                }
                unset($js_file);
            }
            unset($i,$jsroot);
            return $js_links;
        }
        // javascript pre-load
        if ($type == 'js_pre')
        {
            return $this->header_code['pre'];
        }
        // javascript post-load
        if ($type == 'js_post')
        {
            return $this->header_code['post'];
        }
    }

    // SET LAST MODIFIED
    public function set_last_modified ($time = 0, $convert = false)
    {
        // set to current time
        if ($time === -1)
        {
            debug("global", "last-modified: " . gmdate("D, d M Y H:i:s", time()) . " GMT");
            $this->last_modified = time();
            return;
        }

        // convert time from string
        if ($convert)
            $time = strtotime($time);

        // if time is newer then update last modified
        if ($time > $this->last_modified)
        {
            debug("global", "last-modified: " . gmdate("D, d M Y H:i:s", $time) . " GMT");
            $this->last_modified = $time;
        }
    }

    // HTML BR
    public function br ($count = 1)
    {
        return str_repeat("<br />", $count);
    }

    // HTML IMG Tag
    public function img ($src, $align = "", $alt = " ", $width = null, $height = null, $extra = null)
    {
        if ($align) $align = ' align="'.$align.'"';
        if ($alt) $alt = ' alt="'.$alt.'"';
        if ($extra) $extra = ' '.$extra;
        if ($src and is_file($this->_file_root."/images/".$src))
        {
            // load image from images dir
            $size = getimagesize($this->_file_root."/images/".$src);
            if ($size[3])
                $size = ' '.$size[3];
            return '<img src="'.$this->get_root()."/images/".$src.'"'.$size.$align.$extra.$alt.' />';
        }
        else if ($src)
        {
            // load other image
            if ($width) $width = ' width="'.$width.'"';
            if ($height) $height = ' height="'.$height.'"';
            return '<img src="'.$src.'"'.$width.$height.$align.$extra.$alt.' />';
        }
    }

    // HTML A HREF
    public function ahref ($label, $url = "", $extra = "")
    {
        if (!$label and !$url)
            return " &nbsp; ";
        if ($extra)
            $extra = " {$extra}";
        if (!$label and $url)
        {
            if (preg_match('/\@/', $url))
            {
                return "<a href=\"mailto:{$url}\"{$extra}>{$url}</a>";
            }
            else
            {
                return "<a href=\"{$url}\"{$extra}>{$url}</a>";
            }
        }
        else
        {
            return "<a href=\"{$url}\"{$extra}>{$label}</a>";
        }
    }

    // HTML B (bold)
    public function b ($str, $extra = null)
    {
        if ($extra) { $extra = " ".$extra; }
        return "<b".$extra.">".$str."</b>";
    }

    // HTML I (italics)
    public function i ($str, $extra = null)
    {
        if ($extra) { $extra = " ".$extra; }
        return "<i".$extra.">".$str."</i>";
    }

    // HTML Hn (header text)
    public function h ($n, $str, $extra = null)
    {
        if ($extra) { $extra = " ".$extra; }
        return "<H$n".$extra.">".$str."</H$n>";
    }

    // HTML BLOCKQOUTE (indent)
    public function blockquote ($str, $extra = null)
    {
        if ($extra) { $extra = " ".$extra; }
        return "<blockquote".$extra.">".$str."</blockquote>";
    }

    // HTML SMALL (small text)
    public function small ($str, $extra = null)
    {
        if ($extra) { $extra = " ".$extra; }
        return "<small".$extra.">".$str."</small>";
    }

    // HTML P
    public function p ($str = "&nbsp;", $extra = null)
    {
        if ($extra) { $extra = " ".$extra; }
        return "<p".$extra.">".$str."</p>";
    }

    // HTML DIV
    public function div ($str = "&nbsp;", $extra = null)
    {
        if ($extra) { $extra = " ".$extra; }
        return "<div".$extra.">$str</div>";
    }

    // HTML SPAN
    public function span ($str = "&nbsp;", $extra = null)
    {
        if ($extra) { $extra = " ".$extra; }
        return "<span".$extra.">".$str."</span>";
    }

    // HTML SUP
    public function sup ($str = "", $extra = null)
    {
        if ($extra) { $extra = " ".$extra; }
        return "<sup".$extra.">".$str."</sup>";
    }

    // HTML PRE
    public function pre ($str = "&nbsp;", $extra = null)
    {
        if ($extra) { $extra = " ".$extra; }
        return "<pre".$extra.">".$str."</pre>";
    }

    // HTML META
    public function meta ($name = "", $content = "")
    {
        if ($name)
            return '<meta name="'.$name.'" content="'.$content.'">';
        return "";
    }

    // HTML UL
    public function ul ($str = "", $extra = null)
    {
        if (is_array($str))
            $str = join("", array_map(array($this, "li"), $str));
        if ($extra) $extra = " {$extra}";
        return "<ul{$extra}>{$str}</ul>";
    }

    // HTML OL
    public function ol ($str = "", $extra = null)
    {
        if (is_array($str))
            $str = join("", array_map(array($this, "li"), $str));
        if ($extra) $extra = " {$extra}";
        return "<ol{$extra}>{$str}</ol>";
    }

    // HTML LI
    public function li ($str = "", $extra = null)
    {
        if ($extra) $extra = " {$extra}";
        return "<li{$extra}>{$str}</li>";
    }

    // encode text for use in a form field. Removes HTML reserved characters. (same as htmlspecialchars())
    public function encode ($str)
    {
        $str = str_replace('&', '&amp;', $str);
        $str = str_replace('\'', '&#039;', $str);
        $str = str_replace('"', '&quot;', $str);
        $str = str_replace('<', '&lt;', $str);
        $str = str_replace('>', '&gt;', $str);
        return $str;
    }

    // decode text encoded with the encode() function
    public function decode ($str)
    {
        $str = str_replace('&gt;', '>', $str);
        $str = str_replace('&lt;', '<', $str);
        $str = str_replace('&quot;', '\"', $str);
        $str = str_replace('&#039;', '\'', $str);
        $str = str_replace('&amp;', '&', $str);
        return $str;
    }

    // HTML2TXT (convert HTML to text format)
    public function html2txt ($str)
    {
        // remove HEADER junk, CSS, JS, and CDATA
        $str = preg_replace("'<head[^>]*>.*</head>'siU", '', $str);
        $str = preg_replace("'<style[^>]*>.*</style>'siU", '', $str);
        $str = preg_replace("'<script[^>]*>.*</script>'siU", '', $str);
        $str = preg_replace('@<![\s\S]*?--[ \t\n\r]*>@', '', $str);
        // P with closing tags replacement
        $str = preg_replace("'<p[^>]*>(.*)</p>'siU", "\\1\n\n", $str);
        // replace BR and single P tags
        $str = preg_replace('/<br\s*\/?>/i', "\n", $str);
        $str = preg_replace('/<p\s*\/?>/i', "\n\n", $str);
        // strip all other tags
        $str = preg_replace('@<[\/\!]*?[^<>]*?>@si', '', $str);
        // convert entities
        $str = preg_replace("/&nbsp;/", " ", $str);
        $str = html_entity_decode($str, null, "UTF-8");
        // remove extra whitespace
        $str = preg_replace("/\t/", "    ", $str);
        $str = preg_replace("/\r/", "", $str);
        $str = preg_replace("/ {1,}\n/sU", "\n", $str);
        $str = preg_replace("/\n{3,}/sU", "\n\n", $str);
        $str = preg_replace("/\n{4,}/sU", "", $str);
        // return text
        return $str;
    }

    // URLIFY (make urls hyperlinks)
    public function urlify ($text)
    {
        // the regular expression used to match URLs
        $url_regex = '/((http|https|ftp|rss):\/\/'.                                         // protocol
                     '(([a-z0-9$_\.\+!\*\'\(\),;\?&\=\-]|%[0-9a-f]{2})+'.                   // username
                     '(:([a-z0-9$_\.\+!\*\'\(\),;\?&\=\-]|%[0-9a-f]{2})+)'.                 // password
                     '@)?(?#'.                                                              // auth requires @
                     ')((([a-z0-9]\.|[a-z0-9][a-z0-9-]*[a-z0-9]\.)*'.                       // domain segments AND
                     '[a-z][a-z0-9-]*[a-z0-9]'.                                             // top level domain  OR
                     '|((\d|[1-9]\d|1\d{2}|2[0-4][0-9]|25[0-5])\.){3}'.
                     '(\d|[1-9]\d|1\d{2}|2[0-4][0-9]|25[0-5])'.                             // IP address
                     ')(:\d+)?'.                                                            // port
                     ')(((\/+([a-z0-9$_\.\+!\*\'\,\;\:@&\=\-\~]|%[0-9a-f]{2})*)*'.          // path
                     '(\?([a-z0-9$_\.\+!\*\',;:@&\=\-\/]|%[0-9a-f]{2})*)'.                  // query string
                     '?)?)?'.                                                               // path and query string optional
                     '(#([a-z0-9$_\.\+!\*\',;:@&\=\-]|%[0-9a-f]{2})*)?'.                    // fragment
                     ')/i';

        // only use if text has links in it
        if (preg_match($url_regex, $text))
        {
            // extract existing HTML so it is left unprocessed (for example, bbcode is pre-converted)
            $html_matches = array();
            preg_match_all("/((<a href=.*>.*<\/a>)|(<img .*src=.*>)|<iframe .*src=.*>.*<\/iframe>)/Us", $text, $matches, PREG_PATTERN_ORDER);
            foreach ($matches[0] as $c => $match)
            {
                $html_matches[$c] = $matches[1][$c];
                $text = str_replace("$match", "__HTMLMATCH_{$c}__", $text);
            }

            // perform regular expression and wrap (with special handling for <URL>)
            $text = preg_replace_callback(
                $url_regex,
                function ($m) {
                    $newend = '';
                    $end = preg_replace('/^(.*)(\>|&gt\;)([\.\?\!\;\:\,])?$/', '\\2\\3', $m[1]);
                    if ($end and $end != $m[1]) {
                        $m[1] = preg_replace('/(.*)'.preg_quote($end).'$/', '\\1', $m[1]);
                        $newend = $end;
                    }
                    return "<a href=\"{$m[1]}\" class=\"force-wrap\">{$m[1]}</a>{$newend}";
                },
                $text
            );

            // re-insert HTML
            preg_match_all("/(__HTMLMATCH_(\d+)__)/", $text, $matches, PREG_PATTERN_ORDER);
            foreach ($matches[0] as $c => $match)
            {
                $text = str_replace("$match", $html_matches[$c], $text);
            }
        }
        // return linkify'd text
        return $text;
    }

    // EMAILIFY (convert email addresses to links)
    public function emailify ($text, $convert = 0, $shorten = 0)
    {
        // fix quoted printable
        if (preg_match('/^\=/', $text))
        {
            $text = quoted_printable_decode($text);
            $text = mb_convert_encoding($text, 'UTF-8');
            $text = preg_replace('/^\=\?[a-z0-9\-]+\?Q\?(.*)\?\=/', "\\1", $text);
        }
        $emailreg = '/(.*) (\<|\&lt\;)([a-zA-Z0-9_\.\+\/-]+)@([a-zA-Z0-9_\.-]+\.[a-zA-Z0-9]+)(\>|\&gt\;)(.*)/';
        if ($convert and preg_match($emailreg, $text))
        {
            // long format puts the name in and hides the email in the href
            $text = preg_replace_callback(
                "{$emailreg}",
                function ($m) {
                    return "<a href=\"mailto:{$m[3]}@{$m[4]}\">{$m[1]}</a>";
                },
                $text
            );
        }
        else
        {
            // regular emails
            $emailreg = '/([a-zA-Z0-9_\.\+\/-]+)@([a-zA-Z0-9_\.-]+\.[a-zA-Z0-9]+)/';
            if (preg_match($emailreg, $text))
            {
                $text = preg_replace_callback(
                    "{$emailreg}",
                    function ($m) use ($shorten) {
                        if ($shorten)
                            return "<a href=\"mailto:{$m[1]}@{$m[2]}\" class=\"force-wrap\">{$m[1]}</a>";
                        else
                            return "<a href=\"mailto:{$m[1]}@{$m[2]}\" class=\"force-wrap\">{$m[1]}@{$m[2]}</a>";
                    },
                    $text
                );
            }
        }
        return $text;
    }

    // BUILD URLARG (build a valid URL from a list of name/values)
    public function build_urlarg ($vars, $skip = null, $array = null)
    {
        // if not a list, try converting it into a list
        if (!is_array($vars))
        {
            list($url,$params) = preg_split('/\?/', $vars, 2);
            if (preg_match('/;/', $params))
            {
                $urls = preg_split('/;/',$params);
                $vars = array();
                foreach ($urls as $pair)
                {
                    list($key,$value) = preg_split('/=/',$pair);
                    $vars[$key] = $value;
                }
            }
            else
            {
                // return nothing, as there is no vars
                return;
            }
        }

        // loop through vars and remove any we do not want, then build url
        $arr = array();
        foreach ($vars as $key => $val)
        {
            // skip COOKIE vars
            if (isset($_COOKIE[$key]))
               continue;

            if ($skip and gettype($skip) == "array")
            {
                // simple in array check for strings
                if (in_array($key, $skip))
                    continue;
                // support for nested array keys
                foreach ($skip as $skip_ar)
                {
                    // if skip is an array and the array is the correct key check values of skip_ar
                    if (is_array($skip_ar) and isset($skip_ar[$key]))
                    {
                        $val_keys = array_keys($val);
                        $skip_in = array_values($skip_ar);
                        if (in_array($skip_in[0], $val_keys))
                        {
                            continue 2;
                        }
                    }
                }

            }
            else if ($skip and $key == $skip)
            {
                // non arrays, just match key to skip value
                continue;
            }

            if ($array)
                $key = $array."[".$key."]";

            if(is_array($val))
            {
                foreach ($val as $idx => $value)
                {
                    $arr[] = rawurlencode($key."[".$idx."]")."=".rawurlencode($value);
                }
            }
            else
            {
                $arr[] = $key."=".rawurlencode($val);
            }
        }
        return implode(";", $arr);
    }

    // GEN PASSWD (generate a password for user form, etc)
    public function gen_passwd ($pass_len = 8)
    {
        $nps = "";
        mt_srand ((double) microtime() * 1000000);
        while (strlen($nps)<$pass_len)
        {
            $c = chr(mt_rand (0,255));
            if (in_array(strtolower($c), array('0', 'o' ,'i', 'l')))
                continue;
            if (preg_match("/[a-z0-9]/i", $c)) $nps = $nps.$c;
        }
        return ($nps);
    }

    // PERM LINK
    public function gen_perm_link ($str)
    {
        $str = preg_replace('/\s/', '12WSPC21', $str);
        $str = preg_replace('/\W/', '12WSPC21', $str);
        $str = preg_replace('/12WSPC21/', '-', $str);
        $str = preg_replace('/-{2,}/', '-', $str);
        $str = strtolower($str);
        return $str;
    }

    // LOAD_TRANS (load a new translation table)
    //    $table - table to load
    //    $lang  - language override
    public function load_trans ($table, $lang = null)
    {
        // set language
        if (empty($lang))
            $lang = $this->lang;

        // if table is loaded already, return
        if (isset($this->trans_table[$lang][$table]))
            return;

        // init XMLToArray
        check_and_require("XMLToArray");
        $xml = new XMLToArray();

        // load table XML (fallback to english)
        if (file_exists("{$this->_file_root}/templates/{$lang}/global/xml/trans/{$table}.xml"))
            $xml->filename = "{$this->_file_root}/templates/{$lang}/global/xml/trans/{$table}.xml";
        else if ($lang != "en" and file_exists("{$this->_file_root}/templates/en/global/xml/trans/{$table}.xml"))
            $xml->filename = "{$this->_file_root}/templates/en/global/xml/trans/{$table}.xml";
        else
            trigger_error("Unable to load translation XML", E_USER_ERROR);

        // parse XML into array
        $data = $xml->parse();

        // parse the translation table
        $this->trans_table[$lang][$table] = $this->parse_trans_array($data['trans']['#']['str']);

        // debug load trans
        debug("func", "html:load_trans() -> table[{$lang}:{$table}]");

        // done
        return 1;
    }

    // PARSE TRANS ARRAY (nested translation array parser)
    //    $data - XML string array
    private function parse_trans_array ($data)
    {
        // array for storage
        $arr = array();
        // loop and add strings
        foreach ($data as $in)
        {
            if (is_array($in['#']))
            {
                // nested elements, continue parsing tree
                $arr[$in['@']['name']] = $this->parse_trans_array($in['#']['str']);
            }
            else
            {
                // single element, return it
                $arr[$in['@']['name']] = $in['#'];
            }
        }
        // return final array
        return $arr;
    }

    // UNLOAD_TRANS (clear a translation table from memory)
    //    $table - table to unload
    public function unload_trans ($table)
    {
        debug("func", "html:unload_trans() -> table[{$table}]");
        unset($this->trans_table[$this->lang][$table]);
        return true;
    }

    // TRANS (return a processed string from the translastion table)
    //    $table - table to load string from
    //    $str   - name of str to load
    //    $vars  - array of vars to load into string
    public function trans ($table = "global", $str = "", $vars = array())
    {
        // language override
        if (preg_match('/:/', $table))
            list($lang, $table) = preg_split('/:/', $table, 2);
        else
            $lang = $this->lang;

        debug("func", "html:trans() -> table[{$lang}:{$table}] str[{$str}]");
        $this->load_trans($table, ($lang == $this->lang ? null : $lang));
        $out = $this->trans_table[$lang][$table][$str];
        if (count($vars) > 0)
        {
            foreach ($vars as $x => $val)
            {
                $out = str_replace('{$'.$x.'}', $val, $out);
            }
        }
        return $out;
    }

    // TRANS VAL (return a single value of a nested trans array)
    //    $table - table to load string from
    //    $str   - name of str array
    //    $val   - value to return from array
    //    $vars   = array of vars to load into string
    public function trans_val ($table, $str, $val, $vars = array())
    {
        // language override
        if (preg_match('/:/', $table))
            list($lang, $table) = preg_split('/:/', $table, 2);
        else
            $lang = $this->lang;

        debug("func", "html:trans_val() -> table[{$table}] str[{$str}] val[{$val}]");
        $this->load_trans($table, ($lang == $this->lang ? null : $lang));
        if (is_array($this->trans_table[$lang][$table][$str]) and $this->trans_table[$lang][$table][$str][$val])
        {
            $out = $this->trans_table[$lang][$table][$str][$val];
            if (count($vars) > 0)
            {
                foreach ($vars as $x => $val)
                {
                    $out = str_replace('{$'.$x.'}', $val, $out);
                }
            }
            return $out;
        }
        return "";
    }

    // GET TRANS (return a single element of trans array, no further processing, used for translation arrays)
    //    $table - table to load string from
    //    $str   - name of str (array) to return
    public function get_trans ($table, $str)
    {
        debug("func", "html:get_trans() -> table[{$table}] str[{$str}]");
        $this->load_trans($table);
        return $this->trans_table[$this->lang][$table][$str];
    }

    // GET_TRANS_TABLE (return an entire trans table, also load if not loaded)
    //    $table - trans table
    public function get_trans_table ($table)
    {
        $this->load_trans($table);
        return $this->trans_table[$this->lang][$table];
    }

    // TEMPLATE (read a template file and fill in the variables)
    public function template ($theme = null, $template, $vars = null, $noremovetags = 0)
    {
        global $config;

        // debug
        debug("template", "loading template: theme:[{$theme}] lang: [{$this->lang}] template:[{$template}]");

        $file = '';
        $in = '';

        // theme determines where template is loaded from
        switch ($theme)
        {
            // load from global
            case "global":
                if (file_exists("{$this->_file_root}/templates/global/{$template}.template"))
                    $file = "{$this->_file_root}/templates/global/{$template}.template";
                break;

            // load template from theme
            default:
                if (file_exists("{$this->_file_root}/templates/{$this->lang}/{$template}.template"))
                    $file = "{$this->_file_root}/templates/{$this->lang}/{$template}.template";
                else if (($config->lang != $this->lang) and file_exists("{$this->_file_root}/templates/{$config->lang}/{$template}.template"))
                    $file = "{$this->_file_root}/templates/{$config->lang}/{$template}.template";
                break;
        }

        // set last mofified if newer
        if (!empty($file))
        {
            $this->set_last_modified(filemtime($file));
        }

        // where to load data from for template
        if (!empty($this->template_cache[$template]))
        {
            // from cache
            $in = $this->template_cache[$template];
        }
        else if (!empty($file))
        {
            // from file
            $in = join("",file($file));
        }

        // oops not found, load 404 template
        if (empty($in))
        {
            $this->in404 = 1;
            $this->set_last_modified(-1);
            if (file_exists("{$this->_file_root}/templates/{$this->lang}/global/404.template"))
                $in .= join("",file("{$this->_file_root}/templates/{$this->lang}/global/404.template"));
            else if (file_exists("{$this->_file_root}/templates/{$config->lang}/global/404.template"))
                $in .= join("",file("{$this->_file_root}/templates/{$config->lang}/global/404.template"));
            else
                $in .= $this->h1("404 Not Found!");
        }

        // cache this template to save on i/o
        if (empty($this->template_cache[$template]))
            $this->template_cache[$template] = $in;

        // return the text with the vars replaced
        return $this->template_replace($in, $vars, $noremovetags);
    }

    // TEMPLATE_REPLACE (does the substitution for TEMPLATE)
    public function template_replace ($in = "", $vars = array(), $noremovetags = 0)
    {
        // original in (avoid duplicate replacements)
        $orig = $in;

        // remove REM blocks (they are considered a developer remark/comment, and not outputted to user)
        if (preg_match('/<!--REM:\[(.+)\]-->/Us', $orig, $arr))
        {
            $in = preg_replace('/<!--REM:\[(.+)\]-->\n/Us', '', $in);
        }

        // add global system vars
        $vars['root'] = $this->get_root();
        $vars['base_url'] = $this->base_url();
        $vars['self'] = isset_or($_SERVER['PHP_SELF'], '');
        $vars['self_full'] = isset_or($_SERVER['PHP_SELF_FULL'], '');
        $vars['file_root'] = "file://{$this->_file_root}";
        $vars['request_uri'] = isset_or($_SERVER['REQUEST_URI'], '');
        $vars['request_delim'] = ($_GET ? ';' : (preg_match('/\?/', isset_or($_SERVER['REQUEST_URI'],'')) ? '' : '?'));
        $vars['server_name'] = isset_or($_SERVER['SERVER_NAME'], '');
        $vars['http_referer'] = isset_or($_SERVER['HTTP_REFERER'], '');
        $vars['html_lang'] = $this->lang;
        $vars['curtime_year'] = date('Y', time());

        // website content mirroring
        if ($GLOBALS['config']->use_mirror and $GLOBALS['config']->mirror_root)
            $vars['mirror_root'] = $GLOBALS['config']->mirror_root;
        else
            $vars['mirror_root'] = $vars['base_url'];

        // image root shortcut var set to mirror root
        $vars['imgroot'] = "{$vars['mirror_root']}/images";

        // default user info
        if (isset($GLOBALS['login']->id) and $GLOBALS['login']->id)
        {
            $vars['login_id'] = $GLOBALS['login']->id;
            $vars['login_name'] = $GLOBALS['login']->name;
            $vars['login_email'] = $GLOBALS['login']->email;
            $vars['login_pic'] = ($GLOBALS['login']->pic ?
                                    $this->img($GLOBALS['login']->avatar($GLOBALS['login']->id)) :
                                    $this->img('profile/admin_nopic.gif'));
        }
        $vars['login_country'] = isset_or($GLOBALS['login']->country, 'US');

        // add config string vars
        foreach ($GLOBALS['config'] as $key => $val)
        {
            if (is_string($val))
                $vars["config_{$key}"] = $val;
        }
        unset($key, $val);

        // pre-process var import, used for loading vars into the current template from another one
        if (preg_match('/<!--IMPORT:\[[a-z0-9_\-\/]+\]-->/', $orig))
        {
            preg_match_all('/<!--IMPORT:\[([a-z0-9_\-\/]+)\]-->/', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                $tmpl = $this->template_load("local", $match[1][$i], true);
                preg_match_all('/<!--\$X:\[([a-z0-9_\-\.]+)\=(.+)\]-->/', $tmpl, $inmatch);
                for ($i = 0; $i < count($inmatch[0]); $i++)
                {
                    $vars[$inmatch[1][$i]] = $inmatch[2][$i];
                }
                unset($inmatch);
                $in = str_replace('<!--IMPORT:['.$match[1][$i].']-->', "", $in);
                unset($tmpl);
            }
            unset($match, $i);
        }

        // import $X vars from template (no preprocessing)
        if (preg_match('/<!--\$X:\[.+\]-->/', $orig))
        {
            preg_match_all('/<!--\$X:\[([a-z0-9_\-\.]+)\=(.+)\]-->/', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                $vars[$match[1][$i]] = $match[2][$i];
            }
            unset($match);
            $in = preg_replace('/<!--\$X:\[.+\]-->\n/', '', $in);
        }

        // If VAR is defined or matches a value
        if (preg_match('/<!--IF:\[.*\]\{/i', $in))
        {
            // loop through if blocks
            preg_match_all('/<!--IF:\[\$([a-z0-9_]+)(\s?([=!\>\<]+)\s?["\']([a-z0-9_]+)["\'])?\]\{(.*)(\}else\{(.*)\}|\})-->/iUs', $in, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                $rpl = "";
                if (!empty($match[4][$i]) and !empty($vars[$match[1][$i]]))
                {
                    // var matches defined value
                    switch ($match[3][$i])
                    {
                        case "==":
                            if ($vars[$match[1][$i]] == $match[4][$i])
                                $rpl = $match[5][$i];
                            break;
                        case "!=":
                            if ($vars[$match[1][$i]] != $match[4][$i])
                                $rpl = $match[5][$i];
                            break;
                        case ">":
                            if ($vars[$match[1][$i]] > $match[4][$i])
                                $rpl = $match[5][$i];
                            break;
                        case ">=":
                            if ($vars[$match[1][$i]] >= $match[4][$i])
                                $rpl = $match[5][$i];
                            break;
                        case "<":
                            if ($vars[$match[1][$i]] < $match[4][$i])
                                $rpl = $match[5][$i];
                            break;
                        case "<=":
                            if ($vars[$match[1][$i]] <= $match[4][$i])
                                $rpl = $match[5][$i];
                            break;
                    }
                }
                else if (!empty($vars[$match[1][$i]]))
                {
                    // var is defined
                    $rpl = $match[5][$i];
                }
                else if (!empty($match[7][$i]))
                {
                    // else block if it exists
                    $rpl = $match[7][$i];
                }
                $in = str_replace($match[0][$i], $rpl, $in);
                unset($rpl);
            }
            unset($match, $i);
        }

        // replace vars in template
        // NOTE: using preg_replace() breaks as it wants to interpret '$1' in $val
        foreach ($vars as $key => $val)
        {
            $this->template_var_replace($in, $key, $val);
        }
        unset($key, $val);

        // resave the new orig with the template vars inserted (this allows EXEC other statements to include vars)
        $orig = $in;

        // global translation vars
        if (preg_match('/\{trans\(\$[a-z0-9_]+\)\}/', $orig))
        {
            preg_match_all('/\{trans\(\$([a-z0-9_]+)\)\}/', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                $in = str_replace('{trans($'.$match[1][$i].')}', $this->trans("global", $match[1][$i]), $in);
            }
            unset($match, $i);
        }

        // allow the insertion of GET FORM vars (HTML will be stripped!)
        if (preg_match('/\{\$_GET\[[a-z0-9_]+\]\}/', $orig))
        {
            $CLEAN_GET = $this->clean_input($_GET, true);
            preg_match_all('/\{\$_GET\[([a-z0-9_]+)\]\}/', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                if (isset($CLEAN_GET[$match[1][$i]]))
                    $in = str_replace('{$_GET['.$match[1][$i].']}', $this->encode($CLEAN_GET[$match[1][$i]]), $in);
                else
                    $in = str_replace('{$_GET['.$match[1][$i].']}', "", $in);
            }
            unset($match, $i, $CLEAN_GET);
        }

        // remove all the unset vars
        if ($noremovetags == 0)
        {
            $in = preg_replace('/\{\$[a-z0-9_\-]+\}/', '', $in);
            $in = preg_replace('/\{(lc|encode|purl|trans)\(\$[a-z0-9_\-]+\)\}/', '', $in);
        }

        // import template vars from template (vars to be imported) -- single string version
        if (preg_match('/<!--VAR:\[.+\]-->/', $orig))
        {
            preg_match_all('/<!--VAR:\[([a-z0-9_\-\.]+)\|(.+)\]-->/', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                $this->template_vars[$match[1][$i]] = $match[2][$i];
            }
            unset($match);
            $in = preg_replace('/<!--VAR:\[.+\]-->\n/', '', $in);
        }

        // import template vars from template (vars to be imported) -- multiline string version
        if (preg_match('/<!--VAR:\[.+\]\{.+\}-->/Us', $orig))
        {
            preg_match_all('/<!--VAR:\[([a-z0-9_\-\.]+)\]\{(.+)\}-->/Us', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                $this->template_vars[$match[1][$i]] = $match[2][$i];
            }
            unset($match);
            $in = preg_replace('/<!--VAR:\[.+\]\{.+\}-->\n/Us', '', $in);
        }

        // import template vars from template (vars to be imported) -- multiline string alternate version
        if (preg_match('/<LOAD-VAR var=".+">.+<\/LOAD-VAR>/Us', $orig))
        {
            $rplc = 1;
            preg_match_all('/<LOAD-VAR var="(.+)">(.+)<\/LOAD-VAR>/Us', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                $this->template_vars[$match[1][$i]] = $match[2][$i];
                $in = str_replace($match[0][$i], "", $in, $rplc);
            }
            unset($match, $rplc);
        }

        // import CSS
        if (preg_match('/<!--CSS:\[.+\]-->/', $orig))
        {
            preg_match_all('/<!--CSS:\[(.+)\]-->/', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                if ($this->_view_mode == "ajax")
                {
                    // in ajax mode, add the CSS inline
                    $css = "";
                    $css_file = "{$this->_file_root}/css/{$match[1][$i]}.css";
                    if (file_exists($css_file))
                    {
                        $css = "<style>".file_get_contents($css_file)."</style>\n";
                    }
                    $in = str_replace('<!--CSS:['.$match[1][$i].']-->', $css, $in);
                    unset($css, $css_file);
                }
                else
                {
                    $this->add_header_link($match[1][$i], 'css');
                    $in = str_replace('<!--CSS:['.$match[1][$i].']-->', "", $in);
                }
            }
            unset($match, $i);
        }

        // import JavaScript file
        if (preg_match('/<!--JS:\[.+\]-->/', $orig))
        {
            preg_match_all('/<!--JS:\[(.+)\]-->/', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                if (in_array($this->_view_mode, array("ajax","json")))
                {
                    // in ajax mode, add the js link inline
                    $js = "";
                    $js_file = "{$this->_file_root}/js/{$match[1][$i]}.js";
                    if (file_exists($js_file))
                    {
                        $mtime = filemtime($js_file);
                        $js = "<script src=\"{$this->_web_root}/js/{$match[1][$i]}.js?v={$mtime}\" type=\"text/javascript\"></script>\n";
                        unset($mtime);
                    }
                    $in = str_replace('<!--JS:['.$match[1][$i].']-->', $js, $in);
                    unset($js, $js_file);
                }
                else
                {
                    // in other views, add js link to the page header
                    $this->add_header_link($match[1][$i], 'js');
                    $in = str_replace('<!--JS:['.$match[1][$i].']-->', "", $in);
                }
            }
            unset($match, $i);
        }

        // import JavaScript Block into header
        if (preg_match('/<!--JS:\[(pre|post)\]\{/', $in))
        {
            preg_match_all('/<!--JS:\[(pre|post)\]\{(.*)\}-->/Us', $in, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                $this->add_header_link($match[2][$i], $match[1][$i]);
                $in = str_replace('<!--JS:['.$match[1][$i].']{'.$match[2][$i].'}-->', "", $in);
            }
            unset($match, $i);
        }

        // get page title from template
        // add {!} to not add site name automatically
        if (preg_match('/<!--TITLE:\[(.+)\]({!})?-->/', $orig, $arr))
        {
            $in = preg_replace('/<!--TITLE:\[(.+)\]({!})?-->\n/', '', $in);
            $this->page_title = $arr[1];
            if (!empty($arr[2]))
                $this->force_title = true;
            unset($arr);
        }

        // get RSS link
        if (preg_match('/<!--RSS:\[(.+)\]-->/', $orig, $arr))
        {
            $in = preg_replace('/<!--RSS:\[(.+)\]-->\n/', '', $in);
            $this->rss_link = $arr[1];
            unset($arr);
        }

        // set navbar message
        if (preg_match('/<!--NAVBAR_MSG:\[(.+)\]-->/', $orig, $arr))
        {
            $in = preg_replace('/<!--NAVBAR_MSG:\[(.+)\]-->\n/', '', $in);
            if (!defined("NAVBAR_MSG"))
                define("NAVBAR_MSG", $arr[1]);
            unset($arr);
        }

        // set the meta keywords used for a page
        if (preg_match('/<!--META_KEYWORDS:\[(.+)\]-->/', $orig, $arr))
        {
            $in = preg_replace('/<!--META_KEYWORDS:\[(.+)\]-->\n/', '', $in);
            $this->meta_keywords = $arr[1];
            unset($arr);
        }

        // set the meta description uses for a page
        if (preg_match('/<!--META_DESCRIPTION:\[(.+)\]-->/', $orig, $arr))
        {
            $in = preg_replace('/<!--META_DESCRIPTION:\[(.+)\]-->\n/', '', $in);
            $this->meta_description = $arr[1];
            unset($arr);
        }

        // set the meta og
        if (preg_match('/<!--META_OG:\[(.+)\|(.+)\]-->/', $orig))
        {
            preg_match_all('/<!--META_OG:\[(.+)\|(.+)\]-->/', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                if ($match[1][$i] == "image")
                {
                    if (empty($this->meta_og['image']))
                        $this->meta_og['image'] = array();
                    array_push($this->meta_og['image'], $match[2][$i]);
                }
                else
                {
                    $this->meta_og[$match[1][$i]] = $match[2][$i];
                }
                $in = str_replace('<!--META_OG:['.$match[1][$i].'|'.$match[2][$i].']-->', "", $in);
            }
        }

        // load nested templates the template, pass vars onto nested template
        if (preg_match('/<!--INCLUDE:\[[a-z0-9_\-\/]+\]-->/', $orig))
        {
            preg_match_all('/<!--INCLUDE:\[([a-z0-9_\-\/]+)\]-->/', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                $tmpl = $this->template("local", $match[1][$i], $vars);
                $in = str_replace('<!--INCLUDE:['.$match[1][$i].']-->', "$tmpl", $in);
                unset($tmpl);
            }
            unset($match, $i);
        }

        // load and exec plugins in the template
        //   EXEC:       -> execute inline
        //   EXEC_POST   -> only execute if there has been a form POST
        //   EXEC_GET    -> only execute if there has been a form GET
        if (preg_match('/<!--(EXEC|EXEC_POST|EXEC_GET):\[.+\]-->/', $orig))
        {
            preg_match_all('/<!--(EXEC|EXEC_POST|EXEC_GET):\[(.+)\]-->/', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                // plugin output holder
                $out = "";
                // do not execute EXEC_POST if there has not been a POST
                if ($match[1][$i] == 'EXEC' or ($match[1][$i] == 'EXEC_POST' and $_POST) or ($match[1][$i] == 'EXEC_GET' and $_GET))
                {
                    // module name
                    $_module = $match[2][$i];

                    // split params from module name
                    $_PLUGIN = array();
                    if (preg_match('/\?/', $_module))
                    {
                        $arr = preg_split('/\?/', $_module, 2);
                        $_module = $arr[0];
                        $vars = preg_split("/\;/", $arr[1]);
                        foreach ($vars as $var)
                        {
                            list($a, $b) = preg_split('/\=/', $var, 2);
                            $_PLUGIN[$a] = $b;
                        }
                        unset($arr, $vars, $var, $a, $b);
                    }

                    // load plugin
                    $out = include_plugin($_module, $_PLUGIN);
                    if ($out === false)
                        $out = $this->span("plugin:[{$_module}] not found!", 'class="huge error"');
                    unset($_module, $_PLUGIN);
                }
                // cleanup
                $in = str_replace('<!--'.$match[1][$i].':['.$match[2][$i].']-->', $out, $in);
                unset($out);
            }
            unset($match, $i);
        }

        // load and exec plugins (alternate version, can be nested in IF blocks)
        if (preg_match('/<EXEC-PLUGIN\s?(params="(.*)")?>(.+)<\/EXEC-PLUGIN>/', $orig))
        {
            preg_match_all('/<EXEC-PLUGIN\s?(params="(.*)")?>(.+)<\/EXEC-PLUGIN>/', $orig, $match);
            for ($i = 0; $i < count($match[0]); $i++)
            {
                // plugin output holder
                $out = "";
                // module name
                $_module = $match[3][$i];
                // get params
                $_PLUGIN = array();
                if (!empty($match[2][$i]))
                {
                    $vars = preg_split("/\;/", $match[2][$i]);
                    foreach ($vars as $var)
                    {
                        list($a, $b) = preg_split('/\=/', $var, 2);
                        $_PLUGIN[$a] = $b;
                    }
                    unset($arr, $vars, $var, $a, $b);
                }
                // load plugin
                $out = include_plugin($_module, $_PLUGIN);
                if ($out === false)
                    $out = $this->span("plugin:[{$_module}] not found!", 'class="huge error"');
                unset($_module, $_PLUGIN);
                // cleanup
                $rplc = 1;
                $in = str_replace($match[0][$i], $out, $in, $rplc);
                unset($out, $rplc);
            }
            unset($match, $i);
        }

        // override the page view mode (print, text)
        if (preg_match('/<!--VIEW:\[([\w]+)\]-->/', $orig, $arr))
        {
            $this->_view_mode = strtolower($arr[1]);
            $in = preg_replace('/<!--VIEW:\[([\w]+)\]-->\n/', '', $in);
        }

        // return finished page
        unset($orig);
        return $in;
    }

    // TEMPLATE STRIP - (clean dangerous internal commands from user input)
    public function template_strip ($in)
    {
        $in = preg_replace('/<!--[A-Z]+:\[.+\]-->/', '', $in);
        $in = preg_replace('/<EXEC-PLUGIN.+>.+<\/EXEC-PLUGIN>/', '', $in);
        $in = preg_replace('/<LOAD-VAR.+>.+<\/LOAD-VAR>/', '', $in);
        $in = preg_replace('/\{\$_GET\[[a-z0-9_]+\]\}/', '', $in);
        return $in;
    }

    // TEMPLATE VAR (return a var loaded from a template <!--VAR:[]-->)
    //   $var:      var to load from table
    //   $replace:  hash of vars to insert into loaded string using {$var} format
    public function template_var ($var, $replace = array())
    {
        debug("func", "html:template_var() -> var[{$var}]");
        if ($var and !empty($this->template_vars[$var]))
        {
            $out = $this->template_vars[$var];
            if (!empty($replace) and count($replace) > 0)
            {
                foreach ($replace as $key => $val)
                {
                    $this->template_var_replace($out, $key, $val);
                }
            }
            return $out;
        }
        return "";
    }

    // TEMPLATE VAR REPLACE - (replace a single template var)
    //   $in:       original text
    //   $key:      var key
    //   $val:      var replacement value
    private function template_var_replace (&$in, $key = '', $val = '')
    {
        if (preg_match('/\{\$'.$key.'\}/', $in))
        {
            $in = str_replace('{$'.$key.'}', $val, $in);
        }
        if (preg_match('/\{lc\(\$'.$key.'\\)}/', $in))
        {
            $in = str_replace('{lc($'.$key.')}', str_replace(' ', '_', $this->encode(strtolower($val))), $in);
        }
        if (preg_match('/\{encode\(\$'.$key.'\\)}/', $in))
        {
            $in = str_replace('{encode($'.$key.')}', $this->encode($val), $in);
        }
        if (preg_match('/\{urlencode\(\$'.$key.'\\)}/', $in))
        {
            $in = str_replace('{urlencode($'.$key.')}', urlencode($val), $in);
        }
        if (preg_match('/\{strip_html\(\$'.$key.'\\)}/', $in))
        {
            $in = str_replace('{strip_html($'.$key.')}', trim($this->html2txt($val)), $in);
        }
        if (preg_match('/\{strip_tags\(\$'.$key.'\\)}/', $in))
        {
            $in = str_replace('{strip_tags($'.$key.')}', trim(strip_tags($val)), $in);
        }
        if (preg_match('/\{purl\(\$'.$key.'\\)}/', $in))
        {
            $in = str_replace('{purl($'.$key.')}', $this->gen_perm_link($val), $in);
        }
    }

    // TEMPLATE VAR UNSET (clear out a template var from memory)
    //   args:      template var to unset
    public function template_var_unset ()
    {
        $args = func_get_args();
        foreach ($args as &$var)
        {
            unset($this->template_vars[$var]);
        }
    }

    // HTTP HEADER
    public function http_header ($type = "text/html")
    {
        if (empty($this->last_modified))
            $this->set_last_modified(-1);
        header("Content-type: ".$type."; charset=UTF-8");
        header("Cache-Control: public, max-age=86400");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s", $this->last_modified) . " GMT");
    }

    // REDIRECT (simple httpd header redirect)
    public function redirect ($url = "")
    {
        // clear page output buffer
        $this->clear_buffer();

        // if no URL default to website root
        if (!$url and $GLOBALS['config']->base_url)
        {
            $url = $GLOBALS['config']->base_url;
        }

        // redirect
        if ($url)
        {
            header("Location: ".$url);
            return;
        }

        // if still no URL then trigger error page
        trigger_error('Redirect URL not found!', E_USER_ERROR);
    }

    // VERIFY_REFERER (checks to see if the referrer is set to the website url)
    public static function verify_referer ()
    {
        if (strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']))
            return true;
        else
            return false;
    }

    // CLEAR BUFFER
    public function clear_buffer ()
    {
        $status = ob_get_status();
        for ($c = 0; $c < ($status['level'] + 1); $c++)
        {
            ob_end_clean();
        }
    }

// end html class
}

?>

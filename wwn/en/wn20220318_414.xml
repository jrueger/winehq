<kc>
  <title>Wine Traffic</title>
  <author contact="mailto:wwn@winehq.org">Cressida Silver</author>
  <issue num="414" date="03/17/2022"/>
  <intro>
      <p>
        This is the 414th issue of the World Wine News publication. Its main goal is to inform you of what's going on around Wine.
        Wine is an open source implementation of the Windows API on top of X and Unix. Think of it as a Windows compatibility layer.
        Wine does not require Microsoft Windows, as it is a completely alternative implementation consisting of 100% Microsoft-free code.
      </p>
  </intro>

  <section title="The Curious Case of Phasmophobia">

    <p>
      <i>Phasmophobia</i> is an online co-op psychological horror game available on Steam. In <i>Phasmophobia</i> investigators are tasked with gathering evidence using various pieces of equipment in order to determine the type of spirit they are dealing with. One way players glean knowledge from the game is by speaking directly to the apparitions. No really, you (the player) speak with the phantom itself. To do this, <i>Phasmophobia</i> requires speech recognition software.
    </p>

    <p>
      Why all this talk about <i>Phasmophobia?</i>
    </p>

    <p>
      Wine 7.4 arrived last Friday, and it contains the beginnings of an implementation of the windows.media.speech libraries, which is the groundwork for speech recognition capabilities in Wine. The effort was spearheaded by Bernhard Kölbl, because it seemed if anyone was going to get the spirits in <i>Phasmophobia</i> to listen, it was going to be him. (Though he made it clear that this work wouldn't have been possible without the help of Rémi Bernon, Connor McAdams, Zeb Figura, et al.)
    </p>

    <p>
      Nothing worthwhile is ever easy though... sigh. Just getting started required fixing some parts of Wine that were broken, missing, or yet to be implemented (e.g., the HSTRING implementation, and WIDL compiler). Not only that, the work involves some (relatively) newer components of the Windows API, namely, the Universal Windows Platform (UWP) and Windows Runtime (WinRT). These components require a ton of boilerplate code to get even simple things going, and they function in new and (sometimes) weird ways.
    </p>

    <p>
      Once that was sorted, the next step was to select the speech recognition software. Ultimately, <a href="https://alphacephei.com/vosk/">Alpha Cephei's Vosk</a> was chosen. It is an open-source speech recognition toolkit that supports 20+ languages and dialects. Although Bernhard had never worked with Unixlibs before, he was able to get Vosk functioning with Wine. Hard work really does pay. 😀
    </p>

    <p>
      This is just the beginning of speech recognition in Wine, and you can expect more patches soon!
    </p>
  </section>

  <section title="Bugs: In Memoriam">
    <p>
      <center>
        <table cellspacing="0" cellpadding="3" bordercolor="#222222" border="1">
            <tr>
              <td width="100" align="center">
                <b>Bug ID</b>
              </td>
              <td width="100" align="center">
                <b>Open Date</b>
              </td>
              <td width="100" align="center">
                <b>Days Open</b>
              </td>
              <td width="100" align="center">
                <b>Weeks Open</b>
              </td>
              <td width="100" align="center">
                <b>Months Open</b>
              </td>
              <td width="100" align="center">
                <b>Years Open</b>
              </td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=20296">20296</a>
              </td>
              <td align="center">10/9/09</td>
              <td align="center">4536</td>
              <td align="center">468</td>
              <td align="center">149</td>
              <td align="center">12</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=41053">41053</a>
              </td>
              <td align="center">8/3/16</td>
              <td align="center">2046</td>
              <td align="center">292</td>
              <td align="center">67</td>
              <td align="center">6</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=48508">48508</a>
              </td>
              <td align="center">1/24/20</td>
              <td align="center">777</td>
              <td align="center">111</td>
              <td align="center">25</td>
              <td align="center">2</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=50887">50887</a>
              </td>
              <td align="center">3/30/21</td>
              <td align="center">346</td>
              <td align="center">49</td>
              <td align="center">11</td>
              <td align="center">1</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=52204">52204</a>
              </td>
              <td align="center">12/11/21</td>
              <td align="center">90</td>
              <td align="center">13</td>
              <td align="center">3</td>
              <td align="center">0</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=52476">52476</a>
              </td>
              <td align="center">1/28/22</td>
              <td align="center">42</td>
              <td align="center">6</td>
              <td align="center">1</td>
              <td align="center">0</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=52562">52562</a>
              </td>
              <td align="center">2/17/22</td>
              <td align="center">22</td>
              <td align="center">3</td>
              <td align="center">0</td>
              <td align="center">0</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=52581">52581</a>
              </td>
              <td align="center">2/22/22</td>
              <td align="center">17</td>
              <td align="center">2</td>
              <td align="center">0</td>
              <td align="center">0</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=52596">52596</a>
              </td>
              <td align="center">2/26/22</td>
              <td align="center">13</td>
              <td align="center">2</td>
              <td align="center">0</td>
              <td align="center">0</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=52614">52614</a>
              </td>
              <td align="center">3/1/22</td>
              <td align="center">10</td>
              <td align="center">1</td>
              <td align="center">0</td>
              <td align="center">0</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=52618">52618</a>
              </td>
              <td align="center">3/2/22</td>
              <td align="center">9</td>
              <td align="center">1</td>
              <td align="center">0</td>
              <td align="center">0</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=52626">52626</a>
              </td>
              <td align="center">3/4/22</td>
              <td align="center">7</td>
              <td align="center">1</td>
              <td align="center">0</td>
              <td align="center">0</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=52637">52637</a>
              </td>
              <td align="center">3/8/22</td>
              <td align="center">3</td>
              <td align="center">0</td>
              <td align="center">0</td>
              <td align="center">0</td>
            </tr>
            <tr>
              <td align="center">
                <a href ="https://bugs.winehq.org/show_bug.cgi?id=52639">52639</a>
              </td>
              <td align="center">3/8/22</td>
              <td align="center">3</td>
              <td align="center">0</td>
              <td align="center">0</td>
              <td align="center">0</td>
            </tr>
        </table>
      </center>
    </p>
  </section>
</kc>
